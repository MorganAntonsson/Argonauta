/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is a fork of ui.appDisplay.js/part1
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/appDisplay.js
 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Signals = imports.signals;
const Meta = imports.gi.Meta;
const St = imports.gi.St;

const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const Params = imports.misc.params;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const FileIcon = Extension.imports.fileIcon;
const Dialog = Extension.imports.dialog;
const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;

//var MAX_COLUMNS = 6;
//var MIN_COLUMNS = 4;
//var MIN_ROWS = 4;

var INDICATORS_BASE_TIME = 0.25;
var INDICATORS_ANIMATION_DELAY = 0.125;
var INDICATORS_ANIMATION_MAX_TIME = 0.75;

// Follow iconGrid animations approach and divide by 2 to animate out to
// not annoy the user when the user wants to quit appDisplay.
// Also, make sure we don't exceed iconGrid animation total time or
// views switch time.
var INDICATORS_BASE_TIME_OUT = 0.125;
var INDICATORS_ANIMATION_DELAY_OUT = 0.0625;
var VIEWS_SWITCH_TIME = 0.4;
var INDICATORS_ANIMATION_MAX_TIME_OUT =
    Math.min (VIEWS_SWITCH_TIME,
              IconGrid.ANIMATION_TIME_OUT + IconGrid.ANIMATION_MAX_DELAY_OUT_FOR_ITEM);
var PAGE_SWITCH_TIME = 0.3;
var VIEWS_SWITCH_ANIMATION_DELAY = 0.1;


function clamp(value, min, max) {
    return Math.max(min, Math.min(max, value));
}

var BaseAppView = new Lang.Class({
    Name: 'BaseAppView',
    Abstract: true,

    _init: function(params, gridParams) {
        this.settings = Convenience.getSettings();
        gridParams = Params.parse(gridParams, { xAlign: St.Align.MIDDLE,
                                                columnLimit: this.settings.get_int('max-columns'),
                                                minRows: this.settings.get_int('min-rows'),
                                                minColumns: this.settings.get_int('min-rows'),
                                                fillParent: false,
                                                padWithSpacing: true });
        params = Params.parse(params, { usePagination: false });

        if(params.usePagination)
            this._grid = new IconGrid.PaginatedIconGrid(gridParams);
        else
            this._grid = new IconGrid.IconGrid(gridParams);

        this._grid.connect('key-focus-in', Lang.bind(this, function(grid, actor) {
            this._keyFocusIn(actor);
        }));
        // Standard hack for ClutterBinLayout
        this._grid.actor.x_expand = true;

        this._items = {};
        this._allItems = [];
    },

    _keyFocusIn: function(actor) {
        // Nothing by default
    },

    removeAll: function() {
        this._grid.destroyAll();
        this._items = {};
        this._allItems = [];
    },

    _redisplay: function() {
        this.removeAll();
        this._loadFiles();
    },

    addItem: function(icon) {
        let id = icon.id;
        if (this._items[id] !== undefined)
            return;

        this._allItems.push(icon);
        this._items[id] = icon;
    },

    _doSpringAnimation: function(animationDirection) {
        let showFilesButton
        if (Main.overview._dash.showFilesButton && Main.overview._dash.actor.visible)
            showFilesButton = Main.overview._dash.showFilesButton;
        else if (Main.overview._controls.dash.showFilesButton && Main.overview._controls.dash.actor.visible)
            showFilesButton = Main.overview._controls.dash.showFilesButton;
        else
            showFilesButton = null;
        this._grid.actor.opacity = 255;
        this._grid.animateSpring(animationDirection,
                                 Main.overview.viewSelector.showFilesButton);
    },
    
    animate: function(animationDirection, onComplete) {
        if (onComplete) {
            let animationDoneId = this._grid.connect('animation-done', () => {
                this._grid.disconnect(animationDoneId);
                onComplete();
            });
        }

        if (animationDirection == IconGrid.AnimationDirection.IN) {
            let id = this._grid.actor.connect('paint', () => {
                this._grid.actor.disconnect(id);
                this._doSpringAnimation(animationDirection);
            });
        } else {
            this._doSpringAnimation(animationDirection);
        }
    },

    animateSwitch: function(animationDirection) {
        Tweener.removeTweens(this.actor);
        Tweener.removeTweens(this._grid.actor);

        let params = { time: VIEWS_SWITCH_TIME,
                       transition: 'easeOutQuad' };
        if (animationDirection == IconGrid.AnimationDirection.IN) {
            this.actor.show();
            params.opacity = 255;
            params.delay = VIEWS_SWITCH_ANIMATION_DELAY;
        } else {
            params.opacity = 0;
            params.delay = 0;
            params.onComplete = Lang.bind(this, function() { this.actor.hide(); });
        }

        Tweener.addTween(this._grid.actor, params);
    }
});
Signals.addSignalMethods(BaseAppView.prototype);

var PageIndicatorsActor = new Lang.Class({
    Name:'PageIndicatorsActorBis',
    Extends: St.BoxLayout,

    _init: function() {
        this.parent({ style_class: 'page-indicators',
                      vertical: true,
                      x_expand: true, y_expand: true,
                      x_align: Clutter.ActorAlign.END,
                      y_align: Clutter.ActorAlign.CENTER,
                      reactive: true,
                      clip_to_allocation: true });
    },

    vfunc_get_preferred_height: function(forWidth) {
        // We want to request the natural height of all our children as our
        // natural height, so we chain up to St.BoxLayout, but we only request 0
        // as minimum height, since it's not that important if some indicators
        // are not shown
        let [, natHeight] = this.parent(forWidth);
        return [0, natHeight];
    }
});

var PageIndicators = new Lang.Class({
    Name:'PageIndicatorsBis',

    _init: function() {
        this.actor = new PageIndicatorsActor();
        this._nPages = 0;
        this._currentPage = undefined;

        this.actor.connect('notify::mapped',
                           Lang.bind(this, function() {
                               this.animateIndicators(IconGrid.AnimationDirection.IN);
                           })
                          );
    },

    setNPages: function(nPages) {
        if (this._nPages == nPages)
            return;

        let diff = nPages - this._nPages;
        if (diff > 0) {
            for (let i = 0; i < diff; i++) {
                let pageIndex = this._nPages + i;
                let indicator = new St.Button({ style_class: 'page-indicator',
                                                button_mask: St.ButtonMask.ONE |
                                                             St.ButtonMask.TWO |
                                                             St.ButtonMask.THREE,
                                                toggle_mode: true,
                                                checked: pageIndex == this._currentPage });
                indicator.child = new St.Widget({ style_class: 'page-indicator-icon' });
                indicator.connect('clicked', Lang.bind(this,
                    function() {
                        this.emit('page-activated', pageIndex);
                    }));
                this.actor.add_actor(indicator);
            }
        } else {
            let children = this.actor.get_children().splice(diff);
            for (let i = 0; i < children.length; i++)
                children[i].destroy();
        }
        this._nPages = nPages;
        this.actor.visible = (this._nPages > 1);
    },

    setCurrentPage: function(currentPage) {
        this._currentPage = currentPage;

        let children = this.actor.get_children();
        for (let i = 0; i < children.length; i++)
            children[i].set_checked(i == this._currentPage);
    },

    animateIndicators: function(animationDirection) {
        if (!this.actor.mapped)
            return;

        let children = this.actor.get_children();
        if (children.length == 0)
            return;

        for (let i = 0; i < this._nPages; i++)
            Tweener.removeTweens(children[i]);

        let offset;
        if (this.actor.get_text_direction() == Clutter.TextDirection.RTL)
            offset = -children[0].width;
        else
            offset = children[0].width;

        let isAnimationIn = animationDirection == IconGrid.AnimationDirection.IN;
        let delay = isAnimationIn ? INDICATORS_ANIMATION_DELAY :
                                    INDICATORS_ANIMATION_DELAY_OUT;
        let baseTime = isAnimationIn ? INDICATORS_BASE_TIME : INDICATORS_BASE_TIME_OUT;
        let totalAnimationTime = baseTime + delay * this._nPages;
        let maxTime = isAnimationIn ? INDICATORS_ANIMATION_MAX_TIME :
                                      INDICATORS_ANIMATION_MAX_TIME_OUT;
        if (totalAnimationTime > maxTime)
            delay -= (totalAnimationTime - maxTime) / this._nPages;

        for (let i = 0; i < this._nPages; i++) {
            children[i].translation_x = isAnimationIn ? offset : 0;
            Tweener.addTween(children[i],
                             { translation_x: isAnimationIn ? 0 : offset,
                               time: baseTime + delay * i,
                               transition: 'easeInOutQuad',
                               delay: isAnimationIn ? VIEWS_SWITCH_ANIMATION_DELAY : 0
                             });
        }
    }
});
Signals.addSignalMethods(PageIndicators.prototype);

var AllView = new Lang.Class({
    Name: 'AllViewBis',
    Extends: BaseAppView,

    _init: function(directory, source) {
        this.directory = directory;
        this.source = source;
        this.parent({ usePagination: true }, null);
        this._scrollView = new St.ScrollView({ style_class: 'all-apps',
                                               x_expand: true,
                                               y_expand: true,
                                               x_fill: true,
                                               y_fill: false,
                                               reactive: true,
                                               y_align: St.Align.START });
        this.actor = new St.Widget({ layout_manager: new Clutter.BinLayout(),
                                     x_expand:true, y_expand:true });
        this.actor.add_actor(this._scrollView);

        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.EXTERNAL);
        this._adjustment = this._scrollView.vscroll.adjustment;

        this._pageIndicators = new PageIndicators();
        this._pageIndicators.connect('page-activated', Lang.bind(this,
            function(indicators, pageIndex) {
                this.goToPage(pageIndex);
            }));
        this._pageIndicators.actor.connect('scroll-event', Lang.bind(this, this._onScroll));
        this.actor.add_actor(this._pageIndicators.actor);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });
        let box = new St.BoxLayout({ vertical: true });

        this._grid.currentPage = 0;
        this._stack.add_actor(this._grid.actor);
        this._eventBlocker = new St.Widget({ x_expand: true, y_expand: true });
        this._stack.add_actor(this._eventBlocker);

        box.add_actor(this._stack);
        this._scrollView.add_actor(box);

        this._scrollView.connect('scroll-event', Lang.bind(this, this._onScroll));

        let panAction = new Clutter.PanAction({ interpolate: false });
        panAction.connect('pan', Lang.bind(this, this._onPan));
        panAction.connect('gesture-cancel', Lang.bind(this, this._onPanEnd));
        panAction.connect('gesture-end', Lang.bind(this, this._onPanEnd));
        this._panAction = panAction;
        this._scrollView.add_action(panAction);
        this._panning = false;
        this._clickAction = new Clutter.ClickAction();
        this._clickAction.connect('clicked', Lang.bind(this, function() {
            if (!this._currentPopup)
                return;

            let [x, y] = this._clickAction.get_coords();
            let actor = global.stage.get_actor_at_pos(Clutter.PickMode.ALL, x, y);
            if (!this._currentPopup.actor.contains(actor))
                this._currentPopup.popdown();
        }));
        this._eventBlocker.add_action(this._clickAction);

        this._displayingPopup = false;

        this._availWidth = 0;
        this._availHeight = 0;

        //error: Exception in callback for signal: hidden: TypeError: this._getVisibleChildren(...)[firstPageItem] is undefined ....
        //so disable
        /*Main.overview.connect('hidden', Lang.bind(this,
            function() {
                this.goToPage(0);
            }));*/
        this._grid.connect('space-opened', Lang.bind(this,
            function() {
                this._scrollView.get_effect('fade').enabled = false;
                this.emit('space-ready');
            }));
        this._grid.connect('space-closed', Lang.bind(this,
            function() {
                this._displayingPopup = false;
            }));

        this.actor.connect('notify::mapped', Lang.bind(this,
            function() {
                if (this.actor.mapped) {
                    this._keyPressEventId =
                        global.stage.connect('key-press-event',
                                             Lang.bind(this, this._onKeyPressEvent));
                } else {
                    if (this._keyPressEventId)
                        global.stage.disconnect(this._keyPressEventId);
                    this._keyPressEventId = 0;
                }
            }));
        
        if (this.directory) {
            try {
                this.directoryMonitor = this.directory.monitor_directory(0, null);
                this.directoryChangedHandler = this.directoryMonitor.connect('changed', (file, otherFile, eventType) => {
                    try {
                        if (this.directory.query_exists(null))
                            this._redisplay();
                        else
                            this.source.openParentDirectory();
                    } catch(e) {
                        this.source.openHomeDirectory();
                    }
                });
            } catch(e) {
            }
        }
        
        if (this.directory && this.directory.get_path() == GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)) {
            this.volumeMonitor = Gio.VolumeMonitor.get();
            this.mountChangedHandler = this.volumeMonitor.connect('mount-changed', (mount) => {
                this._redisplay();
            });
            this.mountAddedHandler = this.volumeMonitor.connect('mount-added', (mount) => {
                this._redisplay();
            });
        }
        
        this._redisplay();
        //This function sets up a callback to be invoked when either the
        //given actor is mapped, or after some period of time when the machine
        //is idle.  This is useful if your actor isn't always visible on the
        //screen (for example, all actors in the overview), and you don't want
        //to consume resources updating if the actor isn't actually going to be
        //displaying to the user.
        /*this._redisplayWorkId = Main.initializeDeferredWork(this.actor, Lang.bind(this, this._redisplay));*/
    },
    
    disable: function() {
        if (this._keyPressEventId)
                        global.stage.disconnect(this._keyPressEventId);
        if (this.volumeMonitor && this.mountChangedHandler)
            this.volumeMonitor.disconnect(this.mountChangedHandler);
        if (this.volumeMonitor && this.mountAddedHandler)
            this.volumeMonitor.disconnect(this.mountAddedHandler);
        if (this.directoryChangedHandler) {
            this.directoryMonitor.disconnect(this.directoryChangedHandler);
            this.directoryMonitor.cancel();
        }
        this.removeAll();
        this.actor.destroy();
        
    },
    
    _loadFiles: function() {
        let showHiddenFiles = this.settings.get_boolean('show-hidden-files');
        
        //this is the "go back" icon
        this._grid.addItem(new FileIcon.FileIcon(this.directory, this, null, ".."));
        
        //we get a list of gfile in files
        let enumerator;
        //"try" prevent permission error : "Gio.IOErrorEnum: Error opening directory"
        try {
            enumerator = this.directory.enumerate_children('standard::content-type,standard::name', Gio.FileQueryInfoFlags.NONE, null);
        } catch(e) {
            Main.notify("Files View", _("No permission to open") + " " + this.directory.get_path());
            return;
        }
        let files = [];
        let i = 0;
        let fileInfo = enumerator.next_file(null);
        while( fileInfo !== null) {
            let file = enumerator.get_child(fileInfo);
            // ignore hidden files if required, and "application/octet-stream" content-type files
            if ((showHiddenFiles ||
                ( (file.get_basename()[0] !== ".") && !(file.get_basename().endsWith("~") && !GLib.file_test(file.get_path(),GLib.FileTest.IS_DIR)) )) && 
                !Gio.content_type_is_unknown(fileInfo.get_content_type())) {
                files[i] = enumerator.get_child(fileInfo);
                i = i + 1;
            }
            fileInfo = enumerator.next_file(null);
        }
        enumerator.close(null);
        
        //sort with "." and without uppercase
        files.sort(function(a,b) {
                    if ((a.get_basename()[0] === ".") && (b.get_basename()[0] !== ".")) {return 1;}
                    else if ((a.get_basename()[0] !== ".") && (b.get_basename()[0] === ".")) {return -1;}
                    else {return a.get_basename().localeCompare(b.get_basename());}
                  });
                
        for (let j = 0; j < files.length; j++) {
            this._grid.addItem(new FileIcon.FileIcon(files[j], this));
        }
        
        //add mounts if it's Desktop folder
        if (this.volumeMonitor) {
            let mounts = this.volumeMonitor.get_mounts();
            for (let i = 0; i < mounts.length; i++) {
                let file = mounts[i].get_default_location();
                if (!file.get_path()) // (for example blank cd)
                    continue;
                file.mountIcon = mounts[i].get_icon();
                file.mountName = mounts[i].get_name();
                this._grid.addItem(new FileIcon.FileIcon(file, this, null, "mount"));
            }
        }
    },
    
    // Overriden from BaseAppView
    animate: function(animationDirection, onComplete) {
        this._scrollView.reactive = false;
        let completionFunc = () => {
            this._scrollView.reactive = true;
            if (onComplete)
                onComplete();
        };

        if (animationDirection == IconGrid.AnimationDirection.OUT &&
            this._displayingPopup && this._currentPopup) {
            this._currentPopup.popdown();
            let spaceClosedId = this._grid.connect('space-closed', () => {
                this._grid.disconnect(spaceClosedId);
                // Given that we can't call this.parent() inside the
                // signal handler, call again animate which will
                // call the parent given that popup is already
                // closed.
                this.animate(animationDirection, completionFunc);
            });
        } else {
            this.parent(animationDirection, completionFunc);
            if (animationDirection == IconGrid.AnimationDirection.OUT)
                this._pageIndicators.animateIndicators(animationDirection);
        }
    },

    animateSwitch: function(animationDirection) {
        this.parent(animationDirection);

        if (this._currentPopup && this._displayingPopup &&
            animationDirection == IconGrid.AnimationDirection.OUT)
            Tweener.addTween(this._currentPopup.actor,
                             { time: VIEWS_SWITCH_TIME,
                               transition: 'easeOutQuad',
                               opacity: 0,
                               onComplete: function() {
                                  this.opacity = 255;
                               } });

        if (animationDirection == IconGrid.AnimationDirection.OUT)
            this._pageIndicators.animateIndicators(animationDirection);
    },
    
    simpleSwitchIn: function() {
        this.actor.show();
    },
    
    simpleSwitchOut: function() {
        this._pageIndicators.animateIndicators(IconGrid.AnimationDirection.OUT);
        Tweener.removeTweens(this.actor);
        Tweener.removeTweens(this._grid.actor);
        this.actor.hide();
    },

    getCurrentPageY: function() {
        return this._grid.getPageY(this._grid.currentPage);
    },

    goToPage: function(pageNumber) {
        pageNumber = clamp(pageNumber, 0, this._grid.nPages() - 1);

        if (this._grid.currentPage == pageNumber && this._displayingPopup && this._currentPopup)
            return;
        if (this._displayingPopup && this._currentPopup)
            this._currentPopup.popdown();

        let velocity;
        if (!this._panning)
            velocity = 0;
        else
            velocity = Math.abs(this._panAction.get_velocity(0)[2]);
        // Tween the change between pages.
        // If velocity is not specified (i.e. scrolling with mouse wheel),
        // use the same speed regardless of original position
        // if velocity is specified, it's in pixels per milliseconds
        let diffToPage = this._diffToPage(pageNumber);
        let childBox = this._scrollView.get_allocation_box();
        let totalHeight = childBox.y2 - childBox.y1;
        let time;
        // Only take the velocity into account on page changes, otherwise
        // return smoothly to the current page using the default velocity
        if (this._grid.currentPage != pageNumber) {
            let minVelocity = totalHeight / (PAGE_SWITCH_TIME * 1000);
            velocity = Math.max(minVelocity, velocity);
            time = (diffToPage / velocity) / 1000;
        } else {
            time = PAGE_SWITCH_TIME * diffToPage / totalHeight;
        }
        // When changing more than one page, make sure to not take
        // longer than PAGE_SWITCH_TIME
        time = Math.min(time, PAGE_SWITCH_TIME);

        this._grid.currentPage = pageNumber;
        Tweener.addTween(this._adjustment,
                         { value: this._grid.getPageY(this._grid.currentPage),
                           time: time,
                           transition: 'easeOutQuad' });
        this._pageIndicators.setCurrentPage(pageNumber);
    },

    _diffToPage: function (pageNumber) {
        let currentScrollPosition = this._adjustment.value;
        return Math.abs(currentScrollPosition - this._grid.getPageY(pageNumber));
    },

    _onScroll: function(actor, event) {
        if (this._displayingPopup || !this._scrollView.reactive)
            return Clutter.EVENT_STOP;

        let direction = event.get_scroll_direction();
        if (direction == Clutter.ScrollDirection.UP)
            this.goToPage(this._grid.currentPage - 1);
        else if (direction == Clutter.ScrollDirection.DOWN)
            this.goToPage(this._grid.currentPage + 1);

        return Clutter.EVENT_STOP;
    },

    _onPan: function(action) {
        if (this._displayingPopup)
            return false;
        this._panning = true;
        this._clickAction.release();
        let [dist, dx, dy] = action.get_motion_delta(0);
        let adjustment = this._adjustment;
        adjustment.value -= (dy / this._scrollView.height) * adjustment.page_size;
        return false;
    },

    _onPanEnd: function(action) {
         if (this._displayingPopup)
            return;

        let pageHeight = this._grid.getPageHeight();

        // Calculate the scroll value we'd be at, which is our current
        // scroll plus any velocity the user had when they released
        // their finger.

        let velocity = -action.get_velocity(0)[2];
        let endPanValue = this._adjustment.value + velocity;

        let closestPage = Math.round(endPanValue / pageHeight);
        this.goToPage(closestPage);

        this._panning = false;
    },

    _onKeyPressEvent: function(actor, event) {
        if (this._displayingPopup)
            return Clutter.EVENT_STOP;

        if (event.get_key_symbol() == Clutter.Page_Up) {
            this.goToPage(this._grid.currentPage - 1);
            return Clutter.EVENT_STOP;
        } else if (event.get_key_symbol() == Clutter.Page_Down) {
            this.goToPage(this._grid.currentPage + 1);
            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    },

    _keyFocusIn: function(icon) {
        let itemPage = this._grid.getItemPage(icon);
        this.goToPage(itemPage);
    },


    // Called before allocation to calculate dynamic spacing
    adaptToSize: function(width, height) {
        let box = new Clutter.ActorBox();
        box.x1 = 0;
        box.x2 = width;
        box.y1 = 0;
        box.y2 = height;
        box = this.actor.get_theme_node().get_content_box(box);
        box = this._scrollView.get_theme_node().get_content_box(box);
        box = this._grid.actor.get_theme_node().get_content_box(box);
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        let oldNPages = this._grid.nPages();

        this._grid.adaptToSize(availWidth, availHeight);

        let fadeOffset = Math.min(this._grid.topPadding,
                                  this._grid.bottomPadding);
        this._scrollView.update_fade_effect(fadeOffset, 0);
        if (fadeOffset > 0)
            this._scrollView.get_effect('fade').fade_edges = true;

        if (this._availWidth != availWidth || this._availHeight != availHeight || oldNPages != this._grid.nPages()) {
            this._adjustment.value = 0;
            this._grid.currentPage = 0;
            Meta.later_add(Meta.LaterType.BEFORE_REDRAW, Lang.bind(this,
                function() {
                    this._pageIndicators.setNPages(this._grid.nPages());
                    this._pageIndicators.setCurrentPage(0);
                }));
        }

        this._availWidth = availWidth;
        this._availHeight = availHeight;
    }
});
Signals.addSignalMethods(AllView.prototype);


//It's a fork of FrequentView
var RecentView = new Lang.Class({
    Name: 'RecentView',
    Extends: BaseAppView,

    _init: function(source) {
        this.parent(null, { fillParent: true });
        this.source = source;

        this.actor = new St.Widget({ style_class: 'frequent-apps',
                                     layout_manager: new Clutter.BinLayout(),
                                     x_expand: true, y_expand: true });

        this._grid.actor.y_expand = true;
        this.actor.add_actor(this._grid.actor);
        
        // it sync recentview with real activity
        this.actor.connect('notify::mapped', Lang.bind(this, function() {
            if (this.actor.mapped)
                this._redisplay();
        }));
        this._redisplay(); // BaseAppView._redisplay() call _loadFiles()
    },
    
    disable: function() {
        this.removeAll();
        this._grid.actor.destroy();
        this._grid = null;
    },

    
    _loadFiles: function() {
        let recentManager = Gtk.RecentManager.get_default();
        let recentItems = recentManager.get_items(); // recentItems is a array of Gtk.RecentInfo
        recentItems.sort((a, b) => b.get_modified() - a.get_modified()); //sort by date
        
        for (let i = 0; i < Math.min(recentItems.length, 30); i++) {
            let gfile = Gio.File.new_for_uri(recentItems[i].get_uri()); //Gtk.RecentInfo --> uri --> GFile
            if (gfile.query_exists(null)) {  //check the file exist because glib create gfile for no more existing file
                this._grid.addItem(new FileIcon.FileIcon(gfile, this),-1);
            }
        }
        
    },

    // Called before allocation to calculate dynamic spacing
    adaptToSize: function(width, height) {
        let box = new Clutter.ActorBox();
        box.x1 = box.y1 = 0;
        box.x2 = width;
        box.y2 = height;
        box = this.actor.get_theme_node().get_content_box(box);
        box = this._grid.actor.get_theme_node().get_content_box(box);
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        this._grid.adaptToSize(availWidth, availHeight);
    }
});


var FavoriteView = new Lang.Class({
    Name: 'FavoriteView',
    Extends: AllView,

    _init: function(directory, source) {
        this.parent(directory, source);
        this.directory = directory;
        this.source = source;
        this.actor.connect('notify::mapped', Lang.bind(this, function() {
            if (this.actor.mapped)
                this._redisplay();
        }));
     },

    _loadFiles: function() {
        let favorites = this.settings.get_strv('favorite-files');
        for (let j = 0; j < favorites.length; j++) {
            //check that the file still exists in filesystem
            let gfile = Gio.File.new_for_path(favorites[j]);
            if (GLib.file_test(gfile.get_path(),GLib.FileTest.EXISTS)) {
                this._grid.addItem(new FileIcon.FileIcon(gfile, this));
            } else {
                Main.notify("Files View", favorites[j] + " " + _("no longer exists and was removed from favorites"));
                favorites = favorites.filter(a => a !== favorites[j]);
                this.settings.set_strv('favorite-files', favorites);
            }
        }
    }
        

});

var Views = {
    'recent': 0,
    'folder': 1,
    'favorites': 2
};

var ControlsBoxLayout = Lang.Class({
    Name: 'ControlsBoxLayoutBis',
    Extends: Clutter.BoxLayout,

    /**
     * Override the BoxLayout behavior to use the maximum preferred width of all
     * buttons for each child
     */
    vfunc_get_preferred_width: function(container, forHeight) {
        let maxMinWidth = 0;
        let maxNaturalWidth = 0;
        for (let child = container.get_first_child();
             child;
             child = child.get_next_sibling()) {
             let [minWidth, natWidth] = child.get_preferred_width(forHeight);
             maxMinWidth = Math.max(maxMinWidth, minWidth);
             maxNaturalWidth = Math.max(maxNaturalWidth, natWidth);
        }
        let childrenCount = container.get_n_children();
        let totalSpacing = this.spacing * (childrenCount - 1);
        return [maxMinWidth * childrenCount + totalSpacing,
                maxNaturalWidth * childrenCount + totalSpacing];
    }
});

var ViewStackLayout = new Lang.Class({
    Name: 'ViewStackLayoutBis',
    Extends: Clutter.BinLayout,
    Signals: { 'allocated-size-changed': { param_types: [GObject.TYPE_INT,
                                                         GObject.TYPE_INT] } },

    vfunc_allocate: function (actor, box, flags) {
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        // Prepare children of all views for the upcoming allocation, calculate all
        // the needed values to adapt available size
        this.emit('allocated-size-changed', availWidth, availHeight);
        this.parent(actor, box, flags);
    }
});

//It's a fork of AppDisplay
var FilesDisplay = new Lang.Class({
    Name: 'FilesDisplay',

    _init: function(directory) {
        this.settings = Convenience.getSettings();
        this.directory = directory;
        this.oldDirectory = null;
        this.oldFolderView = null;
        this.redisplayTimeout = null;
        this._views = [];
        let view, button;
        let folderLabel = this.directory.get_basename();
        if (folderLabel.length > 25)
            folderLabel = folderLabel.slice(0,23) + "...";
        
        // Only the activeView is loaded when user query it (in _showView)
        //because of loading time and fluidity
        view = null;
        button = new St.Button({ label: folderLabel,
                                 style_class: 'app-view-control button',
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views['folder']] = { 'view': view, 'control': button };
        
        view = null;
        button = new St.Button({ label: _("Recent"),
                                 style_class: 'app-view-control button',
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views['recent']] = { 'view': view, 'control': button };
        
        view = null;
        button = new St.Button({ label: _("Favorites"),
                                 style_class: 'app-view-control button',
                                 can_focus: true,
                                 x_expand: true });
        this._views[Views['favorites']] = { 'view': view, 'control': button };

        this.actor = new St.BoxLayout ({ style_class: 'app-display',
                                         x_expand: true, y_expand: true,
                                         vertical: true });
        this._viewStackLayout = new ViewStackLayout();
        this._viewStack = new St.Widget({ x_expand: true, y_expand: true,
                                          layout_manager: this._viewStackLayout });
        this._viewStackLayout.connect('allocated-size-changed', Lang.bind(this, this._onAllocatedSizeChanged));
        this.actor.add_actor(this._viewStack);
        let layout = new ControlsBoxLayout({ homogeneous: true });
        this._controls = new St.Widget({ style_class: 'app-view-controls',
                                         layout_manager: layout });
        this._controls.connect('notify::mapped', Lang.bind(this,
            function() {
                // controls are faded either with their parent or
                // explicitly in animate(); we can't know how they'll be
                // shown next, so make sure to restore their opacity
                // when they are hidden
                if (this._controls.mapped)
                  return;

                Tweener.removeTweens(this._controls);
                this._controls.opacity = 255;
            }));

        layout.hookup_style(this._controls);
        this.actor.add_actor(new St.Bin({ child: this._controls }));

        for (let i = 0; i < this._views.length; i++) {
            //"this._viewStack.add_actor(this._views[i].view.actor);"
            //move to _showView method
            this._controls.add_actor(this._views[i].control);

            let viewIndex = i;
            this._views[i].control.connect('clicked', Lang.bind(this,
                function(actor) {
                    this._showView(viewIndex);
                   }));
        }
        
        if (this.settings.get_string('start-view') != 'previous') {
            this._showView(Views[this.settings.get_string('start-view')]);
        } else {
            this._showView(Views['folder']);
        }
        this.timeout = Mainloop.timeout_add(60000, () => {
                              Mainloop.source_remove(this.timeout);
                              this.timeout = null;
                          });
    },
    
    disable: function() {
        for (let i = 0; i < this._views.length; i++) {
            if (this._views[i].view)
                this._views[i].view.disable();
        }
        if (this.oldFolderView)
            this.oldFolderView.disable();
            
        this.actor.destroy();
        if (this.timeout)
            Mainloop.source_remove(this.timeout);
    },
    
    animate: function(animationDirection, onComplete) {
        let currentView = this._views.filter(v => v.control.has_style_pseudo_class('checked')).pop().view;

        // Animate controls opacity using iconGrid animation time, since
        // it will be the time the AllView or FrequentView takes to show
        // it entirely.
        let finalOpacity;
        if (animationDirection == IconGrid.AnimationDirection.IN) {
            this._controls.opacity = 0;
            finalOpacity = 255;
        } else {
            finalOpacity = 0
        }

        Tweener.addTween(this._controls,
                         { time: IconGrid.ANIMATION_TIME_IN,
                           transition: 'easeInOutQuad',
                           opacity: finalOpacity,
                          });

        currentView.animate(animationDirection, onComplete);
    },
    
    sizeRedisplay: function() {
        this.iconSize = null;
        this.redisplay();
    },
    
    redisplay: function() {
        if (!this.redisplayTimeout) {
            this._views[this.currentIndex].view.simpleSwitchOut();
            this._viewStack.remove_actor(this._views[this.currentIndex].view.actor);
            this._views[this.currentIndex].view.disable();
        
            //add timeout because it's too fast for viewStack
            let timeout = Mainloop.timeout_add(50, () => {
                        Mainloop.source_remove(timeout);
                        this._createView(this.currentIndex);
                        this._views[this.currentIndex].view.simpleSwitchIn();
            });
            //add redisplayTimeout in case of many redisplay queries in a short time
            this.redisplayTimeout = Mainloop.timeout_add(500, () => {
                        Mainloop.source_remove(this.redisplayTimeout);
                        this.redisplayTimeout = null;
            });
        } else {
            Mainloop.source_remove(this.redisplayTimeout);
            this.redisplayTimeout = Mainloop.timeout_add(500, () => {
                        Mainloop.source_remove(this.redisplayTimeout);
                        this.redisplayTimeout = null;
                        this.redisplay();
            });
        }
    },
    
    switchLeft: function() {
        let activeIndex = (this.currentIndex == 0) ? 2 : this.currentIndex - 1;
        this._showView(activeIndex);
    },
    
    switchRight: function() {
        let activeIndex = (this.currentIndex == 2) ? 0 : this.currentIndex + 1;
        this._showView(activeIndex);
    },
    
    openLocationDialog: function() {
        let locationDialog = new Dialog.LocationDialog(this);
        locationDialog.open();
    },
    
    openHomeDirectory: function() {
        this.openNewDirectory(Gio.File.new_for_path(GLib.get_home_dir())); 
    },
    
    openPreviousDirectory: function() {
        if (this.oldDirectory)
            this.openNewDirectory(this.oldDirectory);
    },
    
    openParentDirectory: function() {
        if (this.mountDirectory && this.directory.get_path() == this.mountDirectory.get_path())
            this.openNewDirectory(Gio.File.new_for_path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)));
        else if (this.directory.get_parent())
            this.openNewDirectory(this.directory.get_parent());
        else if (this.mountDirectory) {
            let basename = this.directory.get_basename();
            let parentDirectoryPath = this.directory.get_path().slice(0, - basename.length);
            this.openNewDirectory(Gio.File.new_for_path(parentDirectoryPath));
        }
    },
    
    openNewMountDirectory: function(newDirectory) {
        this.openNewDirectory(newDirectory);
        this.mountDirectory = newDirectory;
    },
    
    openNewDirectory: function(newDirectory) {
        //newDirectory is a GFile object
        //we keep in memory the previous folder view (often when we browse we go back)
        
        if (this.mountDirectory && newDirectory.get_path().indexOf(this.mountDirectory.get_path()) == -1)
            this.mountDirectory = null;
        // after a time, this previous folder view is removed in order to stay in sync with the filesystem
        if (!this.timeout && this.oldDirectory) {
            this._viewStack.remove_actor(this.oldFolderView.actor);
            this.oldFolderView.disable();
            this.oldFolderView = null;
            this.oldDirectory = null;
        } else if (this.timeout) {
            Mainloop.source_remove(this.timeout);
        }
        this.timeout = Mainloop.timeout_add(30000, () => {
                              Mainloop.source_remove(this.timeout);
                              this.timeout = null;
                          });
                          
        //start hiding the current view before the new view is loaded
        // index 1 : folder view
        if (this.currentIndex == 1 && !this.settings.get_boolean('fade-in-browsing'))
            this._views[this.currentIndex].view.simpleSwitchOut();
        else
            this._views[this.currentIndex].view.animateSwitch(IconGrid.AnimationDirection.OUT);
        this._views[this.currentIndex].control.remove_style_pseudo_class('checked');
        
        
        if (this.oldDirectory && (newDirectory.get_path() == this.oldDirectory.get_path())) {
            //switch new and previous view
            let temp = this._views[1].view;
            this._views[1].view = this.oldFolderView;
            this.oldFolderView = temp;
        } else {
            //remove previous view and build the new        
            if (this.oldFolderView) {
                this._viewStack.remove_actor(this.oldFolderView.actor);
                this.oldFolderView.disable();
            }
            this.oldFolderView = this._views[1].view;
            this._views[1].view = new AllView(newDirectory, this);
            this._viewStack.add_actor(this._views[1].view.actor);
        }
        
        let label = newDirectory.get_basename();
        if (label.length > 25)
            label = label.slice(0,23) + "...";
        this._views[1].control.set_label(label);
        this._views[1].control.add_style_pseudo_class('checked');
        if (this.currentIndex == 1 && !this.settings.get_boolean('fade-in-browsing'))
            this._views[this.currentIndex].view.simpleSwitchIn();
        else
            this._views[1].view.animateSwitch(IconGrid.AnimationDirection.IN);
        
        this.currentIndex = 1;
        this.oldDirectory = this.directory;
        this.directory = newDirectory;
    },
    
    _createView: function(activeIndex) {
        if ((activeIndex == 0)) {
            this._views[0].view = new RecentView(this);
            this._viewStack.add_actor(this._views[0].view.actor);
        } else if ((activeIndex == 1)) {
            this._views[1].view = new AllView(this.directory, this);
            this._viewStack.add_actor(this._views[1].view.actor);
        } else if ((activeIndex == 2)) {
            this._views[2].view = new FavoriteView(null, this);
            this._viewStack.add_actor(this._views[2].view.actor);
        }
    },

    _showView: function(activeIndex) {
        if (!this._views[activeIndex].view)
            this._createView(activeIndex);
            
        for (let i = 0; i < this._views.length; i++) {
            if (i == activeIndex) {
                this._views[i].control.add_style_pseudo_class('checked');
                this.currentIndex = i;
            } else {
                this._views[i].control.remove_style_pseudo_class('checked');
            }

            let animationDirection = i == activeIndex ? IconGrid.AnimationDirection.IN :
                                                        IconGrid.AnimationDirection.OUT;
            if (this._views[i].view)
                this._views[i].view.animateSwitch(animationDirection);
        }
    },
    
    switchDefaultView: function() {
        let defaultView = this.settings.get_string('start-view');
        let defaultPath = this.settings.get_string('start-path');
        if ((defaultView == 'folder') && (this.directory.get_path() != defaultPath)) {
            if ((defaultPath.indexOf('~') !== -1) || !Gio.File.new_for_path(defaultPath).query_exists(null))
                defaultPath = GLib.get_home_dir();
            this.openNewDirectory(Gio.File.new_for_path(defaultPath));
        }
        else if ((defaultView != "previous") && (Views[defaultView] != this.currentIndex))
            this._showView(Views[defaultView]); 
    },

    _onAllocatedSizeChanged: function(actor, width, height) {
        let box = new Clutter.ActorBox();
        box.x1 = box.y1 =0;
        box.x2 = width;
        box.y2 = height;
        box = this._viewStack.get_theme_node().get_content_box(box);
        let availWidth = box.x2 - box.x1;
        let availHeight = box.y2 - box.y1;
        for (let i = 0; i < this._views.length; i++)
            if (this._views[i].view)
                this._views[i].view.adaptToSize(availWidth, availHeight);
    }
});


