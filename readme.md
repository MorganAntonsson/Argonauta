__Argonauta__
=============

![](https://framagit.org/abakkk/Argonauta/raw/ressources/screenshots/screenshot1.jpg)

Argonauta affiche une vue "Fichiers" comparable à la vue Applications, dont elle reprend le design (même disposition, mêmes classes de style). On y accède grâce à la combinaison `super + F`.
Pour qui a l'habitude d'utiliser la vue Applications (`super + A`), c'est assez naturel.
<br><br>
Argonauta displays a "Files" view similar to the Applications view, whose design is taken from it (same layout, same style classes). It is accessed through the combination `super + F`.
For those who are used to using the Applications view (`super + A`), this is quite natural.
<br><br>

![https://peertube.mastodon.host/videos/watch/405ea037-d80d-4045-b4dc-5360d0c6d005](https://peertube.mastodon.host/static/webseed/405ea037-d80d-4045-b4dc-5360d0c6d005-720.mp4)
<br><br>

Fonctionnalités :

* Déplacement de la vue dans les répertoires
* Menu avec quelques actions élémentaires ("ouvrir avec", ouvrir dans le terminal, exécuter ...)
* Un "onglet" pour les fichier récents
* Un autre pour des favoris, qui ne sont pas ceux de Nautilus et sont propres à l'extension
* Affichage des vignettes (si Nautilus les a déjà généré)
* Accès dans le dossier "Bureau' aux partitions et media montés
* Prise en charge des lanceurs
* De nombreux raccourcis internes dont un qui ouvre une boite de dialogue pour saisir un emplacement (avec complétion et historique)
* Tout se fait en lecture seule, ce n'est pas un gestionnaire de fichier

Features :

* Moving the view in directories
* Menu with some basic actions ("open with", open in a terminal, run ...)
* A "tab" for recent files
* Another for favorites. No risk of overwrite those of Nautilus because they are specific to the extension
* Thumbnails display (if Nautilus has already generated them)
* Mounted partitions and media access in Desktop folder
* Launchers support
* Numerous internal shortcuts, one of which opens dialog to enter location (with completion and history)
* Everything is read-only, this is not a file manager

<br><br>
![](https://framagit.org/abakkk/Argonauta/raw/ressources/screenshots/screenshot3.jpg)
<br><br>

Pour l'installer :

1. Télécharger et décompresser ou cloner le dépôt
2. Placer le répertoire obtenu dans ~/.local/share/gnome-shell/extensions
3. IMPORTANT : changer le nom du répertoire en argonauta@framagit.org
4. Un petit coup de `alt + F2` `r` pour redémarrer Gnome-shell sous Xorg, redémarrer ou se re-loguer sous Wayland
5. Activer l'extension dans Gnome-tweak-tool (Ajustements)
6. `super + F` pour tester (super est la touche Windows pour ceux qui ne savent pas)
7. [https://framagit.org/abakkk/Argonauta/issues](https://framagit.org/abakkk/Argonauta/issues) pour dire que ça ne marche pas


To install :

1. Download and decompress or clone the repository
2. Place the resulting directory in ~/.local/share/gnome-shell/extensions
3. IMPORTANT: change the directory name to argonauta@framagit.org
4. A small shot of `alt + F2` `r` to restart Gnome-shell under Xorg, restart or relogin under Wayland
5. Enable extension in Gnome-tweak-tool
6. `super + F` to test (super is the ©©Windows©© key on bad computers like mine)
7. [https://framagit.org/abakkk/Argonauta/issues](https://framagit.org/abakkk/Argonauta/issues) to say it doesn't work

<br><br>
![](https://framagit.org/abakkk/Argonauta/raw/ressources/screenshots/screenshot4.jpg)
<br><br>

Nautilus est un excellent gestionnaire de fichiers mais on l'utilise le plus souvent pour un simple accès. Le garder ouvert en permanence peut s'avérer pénalisant en terme de ressources système tandis que les ouvertures/fermetures incessantes sont des pertes de temps. Par ailleurs une vue Fichiers est mieux adaptée à l'utilisation des espaces de travail.

Nautilus is a great file manager but most often it is used to only access files. Keeping it open is a waste of resources and opening/closing it each time we need a file is a waste of time. Moreover a file view is more suited to multiple work spaces usage.


