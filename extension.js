/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const ExtensionSystem = imports.ui.extensionSystem;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const IconGrid = imports.ui.iconGrid;
const Lang = imports.lang;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const FilesDisplay = Extension.imports.filesDisplay;
const HotCorner = Extension.imports.hotCorner;
const Buttons = Extension.imports.buttons;

const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;

let _searchCancelledOld, _a11yFocusPageOld, slideOutOld, _animateInOld, _animateOutOld, displayManager;

//displayManager --> filesDisplay and via _init --> AllView||RecentView||FavoriteView and via _loadFiles --> fileIcon and via popupMenu --> fileIconMenu
//                           via openLocationDialog --> locationDialog
//               --> DashIcon, PanelIcon and HotCorner

//this code comes from Sticky Notes View by Sam Bull, https://extensions.gnome.org/extension/568/notes/
// Replace ViewSelector's to allow notes view to be selected with CtrlAltTab.
function _a11yFocusPage(page) {
    this._showAppsButton.checked = page == this._appsPage;
    if (page == displayManager.filesPage) {displayManager.toggleFilesPage();}
    page.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
}

// allow showing dash in files view
// Wrap dash's slideOut to stop it animating at the wrong time.
function slideOut() {
    if (Main.overview.viewSelector._activePage !== displayManager.filesPage) {
        this._visible = false;
        this._updateTranslation();
    }
}

// Replace ViewSelector's to go back to files page when leaving the searchPage (if the previous page was the files page)
// this._oldPage value is defined in DisplayManager.onPageChanged and DisplayManager.onOverviewShown
function _searchCancelled() {
    if (this._oldPage) {  //added
        this._showPage(this._oldPage);  //added
    } else {  //added
        this._showPage(this._showAppsButton.checked ? this._appsPage
                                                    : this._workspacesPage);
    }  //added
    this._oldPage = null; //added
    if (this._text.text != '')
        this.reset();
}

// add filesPage animation
function _animateIn(oldPage) {
    if (oldPage)
        oldPage.hide();

    this.emit('page-empty');

    this._activePage.show();
    
    if (Main.overview._dash.showFilesButton && Main.overview._dash.actor.visible)
        this.showFilesButton = Main.overview._dash.showFilesButton;
    else if (Main.overview._controls.dash.showFilesButton && Main.overview._controls.dash.actor.visible)
        this.showFilesButton = Main.overview._controls.dash.showFilesButton;
    else
        this.showFilesButton = null;

    if (this._activePage == this._appsPage && (oldPage == this._workspacesPage || oldPage == this._filesPage)) {
        // Restore opacity, in case we animated via _fadePageOut
        this._activePage.opacity = 255;
        this.appDisplay.animate(IconGrid.AnimationDirection.IN);
    } else if (this._activePage == this._filesPage && (oldPage == this._workspacesPage || oldPage == this._appsPage) && this.showFilesButton &&
        Main.overview.viewSelector.fromOverview) {
        // Restore opacity, in case we animated via _fadePageOut
        this._activePage.opacity = 255;
        this.filesDisplay.animate(IconGrid.AnimationDirection.IN);
    } else {
        this._fadePageIn();
    }
}

function _animateOut(page) {
    let oldPage = page;
    
    if (Main.overview._dash.showFilesButton && Main.overview._dash.actor.visible)
        this.showFilesButton = Main.overview._dash.showFilesButton;
    else if (Main.overview._controls.dash.showFilesButton && Main.overview._controls.dash.actor.visible)
        this.showFilesButton = Main.overview._controls.dash.showFilesButton;
    else
        this.showFilesButton = null;
    
    if (page == this._appsPage &&
        this._activePage == this._workspacesPage &&
        !Main.overview.animationInProgress) {
        this.appDisplay.animate(IconGrid.AnimationDirection.OUT, () => {
            this._animateIn(oldPage)
        });
    } else if (page == this._filesPage &&
        this._activePage == this._workspacesPage &&
        this.showFilesButton &&
        !Main.overview.animationInProgress) {
        this.filesDisplay.animate(IconGrid.AnimationDirection.OUT, () => {
            this._animateIn(oldPage)
        });
    } else {
        this._fadePageOut(page);
    }
}


const DisplayManager = new Lang.Class({
    Name: 'DisplayManger',
    
    _init: function() {
        this.settings = Convenience.getSettings();
        this.startPath = this.settings.get_string('start-path');
        if ((this.startPath.indexOf('~') !== -1) || !Gio.File.new_for_path(this.startPath).query_exists(null))
            this.startPath = GLib.get_home_dir();
        this.startGfile = Gio.File.new_for_path(this.startPath);
        
        //if Gnome shell is starting, we wait until it is completed to add icons because of dash extensions
        if (Main.actionMode == Shell.ActionMode.NONE) {
            this.startupCompleteHandler = Main.layoutManager.connect('startup-complete', Lang.bind(this, this.addToggles));
        } else {
            this.addToggles();
        }
        
        this.filesDisplay = new FilesDisplay.FilesDisplay(this.startGfile);
        this.filesPage = Main.overview.viewSelector._addPage(this.filesDisplay.actor, _("Files"),'folder-symbolic');
        this.filesPage.hide();
        this.filesDisplay.page = this.filesPage;  // Give reference to page for focus handling.
        
        Main.overview.viewSelector._filesPage = this.filesPage;
        Main.overview.viewSelector.filesDisplay = this.filesDisplay;
        
        //bind prefs
        this.settings.connect('changed::panel-button', Lang.bind(this, this.addPanelIcon));
        this.settings.connect('changed::panel-button-position', Lang.bind(this, this.addPanelIcon));
        this.settings.connect('changed::panel-button-appearance', Lang.bind(this, this.addPanelIcon));
        this.settings.connect('changed::dash-button', Lang.bind(this, this.addDashIcon));
        this.settings.connect('changed::icon-name', Lang.bind(this, () => { 
                                                    this.addPanelIcon();
                                                    this.addDashIcon();
                                                    }));
        this.settings.connect('changed::hot-corner', Lang.bind(this, this.addHotCorner));
        this.settings.connect('changed::show-hidden-files', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay));
        this.settings.connect('changed::fade-in-browsing', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay));
        this.settings.connect('changed::icons-hover', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay));
        this.settings.connect('changed::view-icon-size-factor', Lang.bind(this.filesDisplay, this.filesDisplay.sizeRedisplay));
        this.settings.connect('changed::max-columns', Lang.bind(this.filesDisplay, this.filesDisplay.sizeRedisplay));
        this.settings.connect('changed::min-rows', Lang.bind(this.filesDisplay, this.filesDisplay.sizeRedisplay));
        this.settings.connect('changed::run-launchers', Lang.bind(this.filesDisplay, this.filesDisplay.redisplay));
        
        this.pageChangedHandler = Main.overview.viewSelector.connect('page-changed', Lang.bind(this, this.onPageChanged));
        this.overviewShownHandler = Main.overview.connect('shown', Lang.bind(this, this.onOverviewShown));
        this.overviewHiddenHandler = Main.overview.connect('hidden', Lang.bind(this, this.removeKeybindings));
        Main.wm.addKeybinding('toggle-files-view',
                          Convenience.getSettings(),
                          Meta.KeyBindingFlags.NONE,
                          Shell.ActionMode.NORMAL |
                          Shell.ActionMode.OVERVIEW,
                          Lang.bind(this, this.toggleFilesPage)
                          );
        
        this.internalShortcuts = {
            'toggle-hidden-files': Lang.bind(this, this.toggleHiddenFiles),
            'go-parent-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openParentDirectory),
            'go-previous-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openPreviousDirectory),
            'go-home-directory': Lang.bind(this.filesDisplay, this.filesDisplay.openHomeDirectory),
            'switch-subview': Lang.bind(this.filesDisplay, this.filesDisplay.switchRight),
            'switch-subview-left': Lang.bind(this.filesDisplay, this.filesDisplay.switchLeft),
            'switch-subview-right': Lang.bind(this.filesDisplay, this.filesDisplay.switchRight),
            'location-dialog': Lang.bind(this.filesDisplay, this.filesDisplay.openLocationDialog),
            'more-columns': Lang.bind(this, this.increaseColumnsNumber),
            'less-columns': Lang.bind(this, this.decreaseColumnsNumber),
            'more-rows': Lang.bind(this, this.increaseRowsNumber),
            'less-rows': Lang.bind(this, this.decreaseRowsNumber),
            'show-hide-dash': Lang.bind(this, this.showHideDash),
            'refresh-view': Lang.bind(this.filesDisplay, this.filesDisplay.sizeRedisplay),
            'toggle-workspace-view': (() => { Main.overview.viewSelector._showPage(Main.overview.viewSelector._workspacesPage); })
        };        
    },
    
    removeKeybindings: function() {
        if (this.activeKeybindings) {
            for (let key in this.internalShortcuts) {
                Main.wm.removeKeybinding(key);
            }
            this.activeKeybindings = false;
        }
    },
    
    removeHotCorner: function() {
        if (this.hotCorner) {
            this.hotCorner.disable();
            this.hotCorner = null;
        }
    },
    
    removePanelIcon: function() {
        if (this.panelIcon) {
            this.panelIcon.disable();
            this.panelIcon = null;
        }
    },
    
    removeDashIcon: function() {
        if (this.dashIcon) {
            ExtensionSystem.disconnect(this.extensionChangedHandler);
            this.dashIcon.disable();
            this.dashIcon = null;
        }
    },
    
    removeToggles: function() {
        this.removeHotCorner();
        this.removePanelIcon();
        this.removeDashIcon();
    },
    
    //this keybindings are added when overview is shown and removed when overview is hidden, cf this._init
    //I could use 'key-press-event' rather than keybindings but Tab key isn't listened
    addKeybindings: function() {
        for (let key in this.internalShortcuts) {
                Main.wm.addKeybinding(key,
                    Convenience.getSettings(),
                    Meta.KeyBindingFlags.NONE,
                    Shell.ActionMode.OVERVIEW,
                    this.internalShortcuts[key]
                );
            }
        this.activeKeybindings = true;
    },
    
    onPageChanged: function() {
        if ((Main.overview.viewSelector._activePage == this.filesPage) && (Main.actionMode == Shell.ActionMode.OVERVIEW) && (!this.activeKeybindings)) {
            this.addKeybindings();
        } else if ((Main.overview.viewSelector._activePage != this.filesPage) && this.activeKeybindings) {
            this.removeKeybindings();
        }
        
        //this is for _searchCancelled()
        if (Main.overview.viewSelector._activePage == this.filesPage) {
            Main.overview.viewSelector._oldPage = this.filesPage;
        } else if (Main.overview.viewSelector._activePage != Main.overview.viewSelector._searchPage) {
            Main.overview.viewSelector._oldPage = null;
        }
    },
    
    onOverviewShown: function() {
        if ((Main.overview.viewSelector._activePage == this.filesPage) && !this.activeKeybindings)
            this.addKeybindings();
            
        //this is for _searchCancelled()
        if (Main.overview.viewSelector._activePage == this.filesPage) {
            Main.overview.viewSelector._oldPage = this.filesPage;
        } else if (Main.overview.viewSelector._activePage != Main.overview.viewSelector._searchPage) {
            Main.overview.viewSelector._oldPage = null;
        }
    },
    
    addHotCorner: function(first_argument) {
        this.removeHotCorner();
        if (this.settings.get_string('hot-corner') !== 'none') {
            this.hotCorner = new HotCorner.HotCorner();
            this.hotCorner._sig_enter_id = this.hotCorner.corner.connect("enter-event", Lang.bind(this, function() {
                if (this.hotCorner.corner.cantrigger) {
                    this.hotCorner._rippleAnimation();
                    this.toggleFilesPage();
                    this.hotCorner.corner.cantrigger = false;
                }
            }));
            this.hotCorner._sig_leave_id = this.hotCorner.corner.connect("leave-event", Lang.bind(this, function() {
                this.hotCorner.corner.cantrigger = true;
            }));
        }
    },
    
    addPanelIcon: function() {
        this.removePanelIcon();
        if (this.settings.get_string('panel-button') != 'none') {
            this.panelIcon = new Buttons.PanelIcon();
            this.panelIcon.button.connect('button-press-event', Lang.bind(this, this.toggleFilesPage));
        }
    },
    
    addDashIcon: function(first_argument) {
        this.removeDashIcon();
        if (this.settings.get_string('dash-button') !== 'none') {
            this.dashIcon = new Buttons.DashIcon();
            this.dashIcon.showFilesIcon.toggleButton.connect('button-press-event', Lang.bind(this, this.toggleFilesPage));
            this.extensionChangedHandler = ExtensionSystem.connect('extension-state-changed', (evt, extension) => {
                if ((['dash-to-panel@jderose9.github.com', 'ubuntu-dock@ubuntu.com', 'dash-to-dock@micxgx.gmail.com'].indexOf(extension.uuid) !== -1) && (extension.state === 1) ) {
                    this.dashIcon.disable();
                    this.dashIcon = new Buttons.DashIcon();
                    this.dashIcon.showFilesIcon.toggleButton.connect('button-press-event', Lang.bind(this, this.toggleFilesPage));
                }
            });
        }
    },
    
    addToggles: function() {
        this.addHotCorner();
        this.addPanelIcon();
        this.addDashIcon();
    },
    
    filesRedisplay: function() {
        this.filesDisplay.redisplay();
    },
    
    toggleHiddenFiles: function() {
        this.settings.set_boolean('show-hidden-files', !this.settings.get_boolean('show-hidden-files'));
    },
    
    increaseColumnsNumber: function() {
        this.settings.set_int('max-columns', Math.min(this.settings.get_int('max-columns') + 1, 14));
    },
    
    decreaseColumnsNumber: function() {
        this.settings.set_int('max-columns', Math.max(this.settings.get_int('max-columns') - 1, 1));
    },
    
    increaseRowsNumber: function() {
        this.settings.set_int('min-rows', Math.min(this.settings.get_int('min-rows') + 1, 12));
    },
    
    decreaseRowsNumber: function() {
        this.settings.set_int('min-rows', Math.max(this.settings.get_int('min-rows') - 1, 2));
    },
    
    showHideDash: function() {
        let dashActor = Main.overview._controls.dash.actor;
        if (dashActor.is_visible()) {
            dashActor.hide();
            dashActor.set_width(1);
        } else {
            dashActor.show();
            this.dashTimeout = Mainloop.timeout_add(50, () => {
                dashActor.set_width(-1);
                Mainloop.source_remove(this.dashTimeout);
                this.dashTimeout = null;
            });
        }
    },
    
    toggleFilesPage: function(actor) {
        if (Main.overview.viewSelector._activePage !== this.filesPage) {
            this.filesDisplay.switchDefaultView();
            if (!Main.overview.visible) {
                Main.overview.viewSelector.fromOverview = false;
                Main.overview.show(); // <=> Main.overview.toggle()
            } else {
                Main.overview.viewSelector.fromOverview = true;
            }
            Main.overview.viewSelector._showAppsButton.checked = false; //if ...checked=true, we can't directly open the application view from the files view
            Main.overview.viewSelector._showPage(this.filesPage);
        } else {
            if (!this.dashIcon || !actor || (actor !== this.dashIcon.showFilesIcon.toggleButton))
                Main.overview.hide(); // <=> Main.overview.toggle()
            else
                Main.overview.viewSelector._showPage(Main.overview.viewSelector._workspacesPage);
        }
    },
    
    disable: function() {
        if (this.dashTimeout)
            Mainloop.source_remove(this.dashTimeout);
        if (this.startupCompleteHandler)
            Main.layoutManager.disconnect(this.startupCompleteHandler);
        Main.overview.viewSelector.disconnect(this.pageChangedHandler);
        Main.overview.disconnect(this.overviewShownHandler);
        Main.overview.disconnect(this.overviewHiddenHandler);
        delete Main.overview.viewSelector._oldPage;
        this.removeToggles();
        this.removeKeybindings();
        Main.wm.removeKeybinding('toggle-files-view');
        Main.ctrlAltTabManager.removeGroup(this.filesDisplay.actor);
        Main.overview.viewSelector.actor.remove_child(this.filesPage);
        this.filesDisplay.disable();
        this.filesPage.destroy();
    }
});


function init() {
    Convenience.initTranslations();
}

function enable() {
    _searchCancelledOld = Main.overview.viewSelector._searchCancelled;
    _a11yFocusPageOld = Main.overview.viewSelector._a11yFocusPage;
    slideOutOld = Main.overview._controls._dashSlider.slideOut;
    _animateOutOld = Main.overview.viewSelector._animateIn = _animateIn;
    _animateInOld = Main.overview.viewSelector._animateOut = _animateOut;
    Main.overview.viewSelector._searchCancelled = _searchCancelled;
    Main.overview.viewSelector._a11yFocusPage = _a11yFocusPage;
    Main.overview._controls._dashSlider.slideOut = slideOut;
    Main.overview.viewSelector._animateIn = _animateIn;
    Main.overview.viewSelector._animateOut = _animateOut;
    displayManager = new DisplayManager();
}

function disable() {
    Main.overview.viewSelector._searchCancelled = _searchCancelledOld;
    Main.overview.viewSelector._a11yFocusPage = _a11yFocusPageOld;
    Main.overview._controls._dashSlider.slideOut = slideOutOld;
    displayManager.disable();
}

