/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is a fork of ui.appDisplay.js/part2
 * https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/appDisplay.js
 */

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GnomeDesktop = imports.gi.GnomeDesktop;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const St = imports.gi.St;

const AppDisplay = imports.ui.appDisplay;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;

var LONG_LABEL_HOVER_TIMEOUT = 500;


var FileIcon = new Lang.Class({
    Name: 'FileIcon',
    Extends: AppDisplay.AppIcon,
    
    _init : function(file, source, iconParams, type) {
        this.settings = Convenience.getSettings();
        this.source = source;
        this.file = file;
        this.actor = new St.Button({ style_class: 'app-well-app',
                                     reactive: true,
                                     button_mask: St.ButtonMask.ONE | St.ButtonMask.TWO,
                                     can_focus: true,
                                     x_fill: true,
                                     y_fill: true});


        this._iconContainer = new St.Widget({ layout_manager: new Clutter.BinLayout(),
                                              x_expand: true, y_expand: true});

        this.actor.set_child(this._iconContainer);

        this.actor._delegate = this;
        
        if (this.isLauncher()) {
            let desktop = Gio.DesktopAppInfo.new_from_filename(this.file.get_path());
            this.launcherIconName = desktop.get_string('Icon');
            this.launcherExecCmd = desktop.get_string('Exec');
            let locales = GLib.get_language_names();
            if (locales[0] && desktop.has_key('Name[' + locales[0] + ']'))
                this.launcherName = desktop.get_string('Name[' + locales[0] + ']');
            else if (locales[1] && desktop.has_key('Name[' + locales[1] + ']'))
                this.launcherName = desktop.get_string('Name[' + locales[1] + ']');
            else if (desktop.has_key('Name'))
                this.launcherName = desktop.get_string('Name');
        }
        
        this.labelText = type == '..' ? /*'● ●'*/'' : (type == 'mount' ? this.file.mountName : (this.launcherName ? this.launcherName : this.file.get_basename()));
        this._createGiconFunction();

        if (!iconParams)
            iconParams = {};
        iconParams['setSizeManually'] = true;
        if (type == '..')
            iconParams['createIcon'] = Lang.bind(this, this._createReturnIcon);
        else if (type == 'mount')
            iconParams['createIcon'] = Lang.bind(this, this._createMountIcon);
        else
            iconParams['createIcon'] = Lang.bind(this, this._createIcon);
        this.icon = new IconGrid.BaseIcon(this.labelText, iconParams);
        
        this.clutterTextWidth = this.icon.label.get_clutter_text().get_width();

        this._iconContainer.add_child(this.icon.actor);
        this.actor.label_actor = this.icon.label;
        this.actor.file = this.file;
        if (type) {this.actor.type = type;}
        
        this.actor.connect('leave-event', Lang.bind(this, this._onLeaveEvent));
        this.actor.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.actor.connect('touch-event', Lang.bind(this, this._onTouchEvent));
        this.actor.connect('clicked', Lang.bind(this, this._onClicked));
        this.actor.connect('popup-menu', Lang.bind(this, this._onKeyboardPopupMenu));
        this.actor.connect('destroy', Lang.bind(this, this._onDestroy));
        
        this._menu = null;
        this._menuManager = new PopupMenu.PopupMenuManager(this);
        this._menuTimeoutId = 0;
        this._stateChangedId = 0;
    },
    
    hideLongLabel: function() {
        this.longLabel.hide();
        this.icon.label.show();
        Main.layoutManager.removeChrome(this.longLabel);
        this.longLabelShown = false;
    },
    
    showLongLabel: function() {
        Main.layoutManager.addChrome(this.longLabel);
        this.icon.label.hide();
        let [x, y] = this.icon.label.get_transformed_position();
        let [a, b] = this.actor.get_transformed_position();
        let [width, height] = this.longLabel.get_transformed_size();
        this.longLabel.set_size(Math.min(width,2*this.source.source.iconSize*0.75), 3*height);
        this.longLabel.get_clutter_text().set_line_wrap(true);
        this.longLabel.get_clutter_text().set_line_wrap_mode(1);
        let actorWidth = this.actor.allocation.x2 - this.actor.allocation.x1;
        let labelWidth = this.longLabel.get_width();
        let xOffset = Math.floor((actorWidth - labelWidth) / 2);
        this.longLabel.set_position(a + xOffset, y);
        this.longLabel.show();
        this.longLabelShown = true;
        this.longLabel.get_clutter_text().set_line_alignment(1);
    },
        
    // icons are not animated in _init because we need this.isThumbnail (from _createIcon)
    _animateIcons: function() {
        if ( (this.settings.get_string('icons-hover') == 'all') || ((this.settings.get_string('icons-hover') == 'thumbnails') && this.isThumbnail) ) {
            this.scale = this.isThumbnail ? 1.5 : 1.3;
            this.actor.connect('enter-event', Lang.bind(this, this._zoom));
            this.actor.connect('leave-event', Lang.bind(this, this._unZoom));
        }   
    },
    
    // longLabel is not created in _init because we need this.source.source.iconSize (from _createIcon)
    // clutterTextWidth is stored in _init, before resizing
    _createLongLabel: function() {
        //if label is ellipsized, show long label on hover
        this.longLabelTimeout = null;
        this.longLabelShown = false;
        if (this.source.source.iconSize && this.clutterTextWidth > this.source.source.iconSize) {
            this.longLabel = new St.Label({ text: this.labelText });
            this.longLabel.hide();
            
            this.actor.connect('notify::hover', () => {
                if (this.actor.hover) {
                    this.longLabelTimeout = Mainloop.timeout_add(LONG_LABEL_HOVER_TIMEOUT, () => {
                        this.showLongLabel();
                        Mainloop.source_remove(this.longLabelTimeout);
                        this.longLabelTimeout =  null;
                    });
                } else {
                    if (this.longLabelTimeout) {
                        Mainloop.source_remove(this.longLabelTimeout);
                        this.longLabelTimeout = null;
                    } else {
                        this.hideLongLabel();
                    }
                }
            });
            
            let id = Main.overview.connect('hiding', () => {
                if (this.longLabelTimeout) {
                    Mainloop.source_remove(this.longLabelTimeout);
                    this.longLabelTimeout = null;
                }
                if (this.longLabelShown)
                    this.hideLongLabel();
            });
            
            this.actor.connect('destroy', () => {
                Main.overview.disconnect(id);
            });
        }
    },
    
    _createGiconFunction: function() {
        let info = this.file.query_info('standard::icon,standard::content-type', 0, null);       
        this.isVideo = (info.get_content_type().indexOf('video') != -1);
        this.isImage = ((info.get_content_type().indexOf('image') != -1) && (info.get_content_type().indexOf('application') == -1));
        let theme = Gtk.IconTheme.get_default();
            
        if (this.launcherIconName && theme.has_icon(this.launcherIconName)) {
            this.giconFunction = () => { return Gio.ThemedIcon.new(this.launcherIconName); };
        } else if (this.isVideo || this.isImage) {
            let thumbnailPath = GnomeDesktop.desktop_thumbnail_path_for_uri(this.file.get_uri(),1); //1: large
            let thumbnailFile = Gio.File.new_for_path(thumbnailPath);
            //check normal thumbnail exists
            if (thumbnailFile.query_exists(null)) {
                this.giconFunction = () => { return Gio.icon_new_for_string(thumbnailPath); };
                this.isThumbnail = true;
                //if the image is an icon, display it but without 16/10 ratio and without wallpaper menu item
                if (this.file.get_path().indexOf('icon') != -1)
                        this.isThumbnail = false;
            //else try with large thumbnail
            } else {
                thumbnailPath = GnomeDesktop.desktop_thumbnail_path_for_uri(this.file.get_uri(),0); //0: normal
                thumbnailFile = Gio.File.new_for_path(thumbnailPath);
                //check large thumbnail exists
                if (thumbnailFile.query_exists(null)) {
                    this.giconFunction = () => { return Gio.icon_new_for_string(thumbnailPath); };
                    this.isThumbnail = true;
                    if (this.file.get_path().indexOf('icon') != -1)
                        this.isThumbnail = false;
                // else else default content-type icon
                } else {
                    this.giconFunction = () => { return info.get_icon(); };
                }
            }
        } else {
            this.giconFunction = () => { return info.get_icon(); };
        }
    },
    
    //It stores iconSize in filesDisplay (this.source.source) to have the good size from the very first pass and prevent "pulse" effect due to resizing
    _createIcon: function(iconSize) {
        iconSize = this.settings.get_double('view-icon-size-factor')*iconSize;        
        let temp = iconSize;
        if (this.source.source.iconSize && !this.icon.secondPass)
            iconSize = this.source.source.iconSize;
        if (this.icon.secondPass)
                this.source.source.iconSize = temp;
                
        let gicon = this.giconFunction();

        //use Gtk.IconTheme to check whether gicon exist in user theme (Adwaita Humanity...)
        let theme = Gtk.IconTheme.get_default();
        let gtkIconInfo = theme.lookup_by_gicon(gicon, iconSize, 0);
        if (!gtkIconInfo)
            gicon = Gio.ThemedIcon.new_from_names(['stock_unknown', 'text-x-script']);
        
        let icon = new St.Icon();
        icon.set_gicon(gicon);
        
        // 16/10 ratio for thumbnails icons
        if (this.isThumbnail) {
            icon.set_icon_size(iconSize*16/10);
            icon.set_style('height:' + iconSize*0.70 + 'px;' + 'width:' + iconSize*0.70*16/10 + 'px;' + 'margin-top:' + iconSize*0.15 + 'px;' + 'margin-bottom:' + iconSize*0.15 + 'px');
        } else {
            icon.set_icon_size(iconSize);
        }
        
        this.icon.label.set_style('margin-top:' + iconSize*0.1 + 'px');
        
        if (this.icon.secondPass) {
            this._createLongLabel();
            this._animateIcons();
        }
        
        this.icon.secondPass = true;
        return icon;
    },
    
    _createMountIcon: function(iconSize) {
        iconSize = this.settings.get_double('view-icon-size-factor')*iconSize;
        let temp = iconSize;
        if (this.source.source.iconSize && !this.icon.secondPass)
            iconSize = this.source.source.iconSize;
        if (this.icon.secondPass)
                this.source.source.iconSize = temp;
            
        let gicon = this.file.mountIcon;
        let icon = new St.Icon();
        icon.set_gicon(gicon);
        icon.set_icon_size(iconSize);
        this.icon.label.set_style('margin-top:' + iconSize*0.1 + 'px');
        if (this.icon.secondPass) {
            this._animateIcons();
            this._createLongLabel();
        }
        this.icon.secondPass = true;
        return icon;
    },
    
    _createReturnIcon: function(iconSize) {
        iconSize = this.settings.get_double('view-icon-size-factor')*iconSize;
        let temp = iconSize;
        if (this.source.source.iconSize && !this.icon.secondPass)
            iconSize = this.source.source.iconSize;
        if (this.icon.secondPass)
                this.source.source.iconSize = temp;
            
        let gicon = new Gio.ThemedIcon({ name: 'go-previous-symbolic' });
        let icon = new St.Icon();
        icon.set_gicon(gicon);
        icon.set_icon_size(iconSize*0.65);
        icon.set_style('margin-top:' + iconSize/8 + 'px;');
        this.icon.actor.set_style('color:gray;');
        this.actor.connect('enter-event', () => {
            this.icon.actor.set_style('background:none;');
        });
        this.actor.connect('leave-event', () => {
            // add timeout because of 'flash' background when leaving button
            let timeout = Mainloop.timeout_add(0, () => {
                this.icon.actor.set_style('color:gray;');
                Mainloop.source_remove(timeout);
            });
        });
        this.icon.label.set_style('margin-top:' + iconSize*0.1 + 'px');
        this.icon.secondPass = true;
        return icon;
    },
    
    _zoom: function() {
        //prevent icon background change when hovering
        this.icon.actor.set_style('background:none;');
        
        this.icon.icon.allocate_preferred_size(0); //that is to get the good size in get_transformed_size
        let [width, height] = this.icon.icon.get_transformed_size();
        this.icon.icon.set_pivot_point(0.5, 0.5);
        let scaledHeight = height * this.scale;
        
        //create a new bigger icon to prevent blur effect
        this.iconClone = new St.Icon({ gicon: this.icon.icon.get_gicon(),
                                      icon_size: this.icon.icon.get_icon_size()*this.scale });
        this.iconClone.set_size(width*this.scale, height*this.scale);
        this.iconClone.opacity = 0;
        Main.uiGroup.add_actor(this.iconClone);
        this.zoomId = this.actor.connect('destroy', () => {
            if (this.iconClone)
                Main.uiGroup.remove_actor(this.iconClone);
        });
    
        Tweener.removeTweens(this.icon.icon);
        Tweener.removeTweens(this.icon.label);
        Tweener.addTween(this.icon.icon,
                         { time: 0.25,
                           scale_x: this.scale,
                           scale_y: this.scale,
                           transition: 'easeOutQuad',
                           onComplete: () => {
                               let [xl, yl] = this.icon.icon.get_transformed_position();
                               this.iconClone.set_position(xl, yl);
                               this.icon.icon.set_opacity(0);
                               this.iconClone.set_opacity(255);
                           }
                        });
        Tweener.addTween(this.icon.label,
                         { time: 0.25,
                           translation_x: 0,
                           translation_y: (scaledHeight - height) / 2,
                           transition: 'easeOutQuad'
                        });
        this.zoomed = true;
    },
    
    _unZoom: function() {
        Tweener.removeTweens(this.icon.icon);
        Tweener.removeTweens(this.icon.label);
        this.icon.icon.set_opacity(255);
        
        Tweener.addTween(this.icon.icon,
                         { time: 0.25,
                           scale_x: 1,
                           scale_y: 1,
                           opacity: 255,
                           transition: 'easeOutQuad',
                        });
        Tweener.addTween(this.icon.label,
                         { time: 0.25,
                           translation_x: 0,
                           translation_y: 0,
                           opacity: 255,
                           transition: 'easeOutQuad'
                        });
        if (this.zoomId && this.actor)
            this.actor.disconnect(this.zoomId);
        
        if (this.iconClone) {
            this.iconClone.set_opacity(0);
            Main.uiGroup.remove_actor(this.iconClone);
            this.iconClone.destroy();  
            this.iconClone = null; 
        }
        this.zoomed = false;
    },
    
    _onClicked: function(actor, button) {
        this._removeMenuTimeout();
        if (actor.type && actor.type == "..") {
            // this.source.source is FilesDisplay
            this.source.source.openParentDirectory();
        } else if (GLib.file_test(actor.file.get_path(),GLib.FileTest.IS_DIR)) {
            if (actor.type && actor.type == 'mount')
                this.source.source.openNewMountDirectory(actor.file);
            else
                this.source.source.openNewDirectory(actor.file);
        } else {
            if (this.launcherExecCmd) {
                try {
                    Util.spawnCommandLine(this.launcherExecCmd);
                    Main.overview.hide();
                } catch(e) {
                    return;
                }
            } else {
                try {
                    Gio.AppInfo.launch_default_for_uri(actor.file.get_uri(), global.create_app_launch_context(0, -1));
                    Main.overview.hide();
                } catch(e) {
                    return;
                }
            }
        }
    },
    
    isLauncher: function() {
        if (this.file.get_basename().slice(-8) != '.desktop')
            return false;
        if (!GLib.file_test(this.file.get_path(),GLib.FileTest.IS_EXECUTABLE))
            return false;
        if ( (this.file.get_parent().get_path() != GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)) && (this.settings.get_string('run-launchers') == 'desktop-dir') )
            return false;
        if (this.settings.get_string('run-launchers') == 'none')
            return false;
        return true;
    },
    
    popupMenu: function() {
        this._removeMenuTimeout();
        this.actor.fake_release();

        if (this._draggable)
            this._draggable.fakeRelease();

        if (!this._menu) {
            this._menu = new FileIconMenu(this);
            this._menu.connect('activate-window', (menu, window) => {
                this.activateWindow(window);
            });
            this._menu.connect('open-state-changed', (menu, isPoppedUp) => {
                if (!isPoppedUp) {
                    if (this.zoomed)
                        this._unZoom()
                    this._onMenuPoppedDown();
                }
            });
            let id = Main.overview.connect('hiding', () => {
                this._menu.close();
            });
            this.actor.connect('destroy', () => {
                Main.overview.disconnect(id);
            });

            this._menuManager.addMenu(this._menu);
        }

        this.emit('menu-state-changed', true);

        this.actor.set_hover(true);
        this._menu.popup();
        this._menuManager.ignoreRelease();
        this.emit('sync-tooltip');

        return false;
    }        
});


var FileIconMenu = new Lang.Class({
    Name: 'FileIconMenu',
    Extends: PopupMenu.PopupMenu,
    
    _init: function(source) {
        this.settings = Convenience.getSettings();
        let side = St.Side.LEFT;
        if (Clutter.get_default_text_direction() == Clutter.TextDirection.RTL)
            side = St.Side.RIGHT;

        this.parent(source.actor, 0.5, side);
        
        // We want to keep the item hovered while the menu is up
        this.blockSourceEvents = true;

        this.source = source;
        this.apps = [];

        this.actor.add_style_class_name('app-well-menu');

        // Chain our visibility and lifecycle to that of the source
        source.actor.connect('notify::mapped', Lang.bind(this, function () {
            if (!source.actor.mapped)
                this.close();
        }));
        source.actor.connect('destroy', Lang.bind(this, this.destroy));

        Main.uiGroup.add_actor(this.actor);
    },
    
    destroy: function() {
        this.parent();
        Main.uiGroup.remove_actor(this.actor);
    },
    
    _appendSeparator: function () {
        let separator = new PopupMenu.PopupSeparatorMenuItem();
        this.addMenuItem(separator);
    },

    _appendMenuItem: function(labelText) {
        // FIXME: app-well-menu-item style
        let item = new PopupMenu.PopupMenuItem(labelText);
        this.addMenuItem(item);
        return item;
    },

    popup: function(activatingButton) {
        this._redisplay();
        this.open();
    },
    
    //here we build all the menu
    _redisplay: function() {
        this.removeAll();
        
        let infoSection = new PopupMenu.PopupMenuSection();
        this.addMenuItem(infoSection);
        this._appendSeparator();
        let actionSection = new PopupMenu.PopupMenuSection();
        this.addMenuItem(actionSection);
        
        let nameItem = new PopupMenu.PopupMenuItem(this.source.file.get_basename(), { hover: false, activate: false });
        nameItem.label.set_style('font-weight:bold');
        infoSection.addMenuItem(nameItem);
        
        
        //short path when it's too long
        let path = this.source.file.get_path();
        if (path.length > 50)
            path = "..." + path.slice(-47,200);
        let pathItem = new PopupMenu.PopupMenuItem(path, { hover: false, activate: false } );
        infoSection.addMenuItem(pathItem);
        pathItem.label.set_style('font-size:0.85em');
        
        
        this.favorites = this.settings.get_strv('favorite-files');
        let favoriteItem;
        if (this.favorites.indexOf(this.source.file.get_path()) !== -1) {
            favoriteItem = new PopupImageMenuItemFixed(_("Remove favorites"), 'starred-symbolic');
            favoriteItem.connect('activate', Lang.bind(this, function() {
                                 this.favorites = this.favorites.filter(a => a !== this.source.file.get_path());
                                 this.settings.set_strv('favorite-files', this.favorites);
                                 // this.source.source.source is filesDisplay, 2 is the favorites view index
                                 if (this.source.source.source.currentIndex == 2) {
                                     this.source.source._redisplay();
                                 } else {
                                     this._redisplay();
                                 }
                             }));
        } else {
            favoriteItem = new PopupImageMenuItemFixed(_("Add to favorites"), 'non-starred-symbolic');
            favoriteItem.connect('activate', Lang.bind(this, function() {
                                 this.favorites.push(this.source.file.get_path());
                                 this.settings.set_strv('favorite-files', this.favorites);
                                 this._redisplay();
                             }));
        }
        actionSection.addMenuItem(favoriteItem);
        
        if (!this.clipIconName)
            this.clipIconName = 'edit-copy-symbolic';
        let clipBoardItem = new PopupImageMenuItemFixed(_("Copy path to clipboard"), this.clipIconName);
        actionSection.addMenuItem(clipBoardItem);
        clipBoardItem.connect('activate', Lang.bind(this, function() {
                              St.Clipboard.get_default().set_text(St.ClipboardType.CLIPBOARD, this.source.file.get_path());
                              // create loop to open the menu with the new icon;
                              this._timeout = Mainloop.timeout_add(0, () => {
                                  this.clipIconName = 'edit-paste-symbolic';
                                  this._redisplay();
                                  this.open();
                                  Mainloop.source_remove(this._timeout);
                              });
                              //there is two clipboard systems, here we use the second.("St.ClipboardType.PRIMARY" for the first)
                          }));
        
        
        
        //if it's not a folder                
        if (!GLib.file_test(this.source.file.get_path(),GLib.FileTest.IS_DIR)) {
            let sourceInfo = this.source.file.query_info('standard::content-type,standard::size', 0, null);

            
            //build size item
            let size = sourceInfo.get_size();
            let sizeLabel = "0";
            if (size < 1000) { sizeLabel = size + " octets";}
            else if (size < 1000000) { sizeLabel = Math.round(size/100)/10 + " ko";}
            else if (size < 1000000000) { sizeLabel = Math.round(size/10000)/100 + " Mo";}
            else { sizeLabel = Math.round(size/10000000)/100 + " Go";}
            let sizeItem = new PopupMenu.PopupMenuItem(sizeLabel, { hover: false, activate: false });
            sizeItem.label.set_style('font-size:0.85em');
            infoSection.addMenuItem(sizeItem);
            
            
            if (this.source.isThumbnail && this.source.isImage) {
                this.bgSetting = new Gio.Settings({ schema_id: 'org.gnome.desktop.background' });
                if (this.oldBgUri && (this.bgSetting.get_string('picture-uri') == this.source.file.get_uri())) {
                    let bgLabel = _("Remove as wallpaper");
                    let bgItem = new PopupImageMenuItemFixed(bgLabel, 'preferences-desktop-screensaver');
                    actionSection.addMenuItem(bgItem);
                    bgItem.connect('activate', Lang.bind(this, function() {
                                      this.bgSetting.set_string('picture-uri', this.oldBgUri);
                                      this.oldBgUri = null;
                                      //I don't understand but that loop is necessary to redisplay
                                      this._timeout = Mainloop.timeout_add(0, () => {
                                          this._redisplay();
                                          this.open();
                                          Mainloop.source_remove(this._timeout);
                                      });
                                  }));
                } else {
                    let bgLabel = _("Set as wallpaper");
                    let bgItem = new PopupImageMenuItemFixed(bgLabel, 'preferences-desktop-wallpaper');
                    actionSection.addMenuItem(bgItem);
                    bgItem.connect('activate', Lang.bind(this, function() {
                                      this.oldBgUri = this.bgSetting.get_string('picture-uri');
                                      this.bgSetting.set_string('picture-uri', this.source.file.get_uri());
                                      //idem
                                      this._timeout = Mainloop.timeout_add(0, () => {
                                          this._redisplay();
                                          this.open();
                                          Mainloop.source_remove(this._timeout);
                                      });
                                  }));
                }
            }
            
            
            //build "open with ..." item
            let contentType = sourceInfo.get_content_type();
            //sometimes default app is no in recommended/all apps list
            let app = Gio.AppInfo.get_default_for_type(contentType,false);
            //fork of LoadApps ...
            let apps = Gio.AppInfo.get_all_for_type(contentType).filter(function(appInfo) {
                try {
                    let id = appInfo; // catch invalid file encodings
                } catch(e) {
                    return false;
                }
                return appInfo.should_show();
            }).map(function(app) {
                return app;
            });
            
            //check whether default app is already in recommended apps list
            if (!apps.some(appsfile => appsfile.get_name() === app.get_name())) {
                apps.unshift(app);
            }
            
            let openWith = new PopupMenu.PopupSubMenuMenuItem(_("Open with ..."));
            actionSection.addMenuItem(openWith);

            let openWithItems = [];
            if (apps[0]) {
                for (let i = 0; i < apps.length; i++) {
                    openWithItems[i] = new PopupImageMenuItemFixed(apps[i].get_name(), apps[i].get_icon().get_names()[0]);
                    openWith.menu.addMenuItem(openWithItems[i]);
                    // self is a trick because old gjs refuse apps[i]
                    let self = [];
                    self.app = apps[i];
                    self.file = this.source.file;
                    openWithItems[i].connect('activate', Lang.bind(self, function() {
                                         self.app.launch([self.file], null);
                                         Main.overview.hide();
                                        }));
                }
            } else {
                openWith.menu.addMenuItem(new PopupMenu.PopupMenuItem(_("No applications for this file")));
            }
            
            //buid execute item
            if (GLib.file_test(this.source.file.get_path(),GLib.FileTest.IS_EXECUTABLE)) {
                let execute = new PopupMenu.PopupSubMenuMenuItem(_("Run"));
                actionSection.addMenuItem(execute);
                
                //I use xterm because i don't know how do it with gnome-terminal
                let executeTermItem = new PopupImageMenuItemFixed(_("Run in a terminal"), 'utilities-x-terminal');
                execute.menu.addMenuItem(executeTermItem);
                executeTermItem.connect('activate', Lang.bind(this, function() {
                                        //Util.spawnCommandLine("xterm -e '" + this.source.file.get_path() +"; $SHELL'");
                                        Util.spawnCommandLine("gnome-terminal --window -e \u0022bash -c " + "'\\" + "\u0022" + this.source.file.get_path() + "\\" + "\u0022'" + ";$SHELL\u0022");
                                        // gnome-terminal --window -e "bash -c '\"/home/folder with spaces/script\"';$SHELL"
                                        Main.overview.hide();
                                     }));
                                     
                let executeBGItem = new PopupImageMenuItemFixed(_("Run in the background"), 'system-run-symbolic');
                execute.menu.addMenuItem(executeBGItem);
                executeBGItem.connect('activate', Lang.bind(this, function() {
                                        Util.spawnCommandLine("'" + this.source.file.get_path() + "'");
                                     }));
            }

        //if it's a folder
        } else {
            let goNautilusItem = new PopupImageMenuItemFixed(_("Open the folder in Nautilus"), "system-file-manager");
            actionSection.addMenuItem(goNautilusItem);
            goNautilusItem.connect('activate', Lang.bind(this, function() {
                                   Util.spawnCommandLine("nautilus " + this.source.file.get_uri());
                                   Main.overview.hide();
                        }));
            
            let goTerminalItem = new PopupImageMenuItemFixed(_("Open the folder in Gnome-terminal"), "utilities-terminal");
            actionSection.addMenuItem(goTerminalItem);
            goTerminalItem.connect('activate', Lang.bind(this, function() {
                                   Util.spawnCommandLine("gnome-terminal --window --working-directory=" + "'" + this.source.file.get_path() + "'");
                                   Main.overview.hide();
                        }));
        }
                    
     } 

});

var PopupImageMenuItemFixed = new Lang.Class({
    Name: 'PopupImageMenuItemFixed',
    Extends: PopupMenu.PopupBaseMenuItem,

    _init: function(text, icon, params) {
        this.parent(params);

        this._icon = new St.Icon({ style_class: 'popup-menu-icon' });
        this.actor.add_child(this._icon);
        this.label = new St.Label({ text: text });
        this.actor.add_child(this.label);
        this.actor.label_actor = this.label;

        this.setIcon(icon);
    },

    setIcon: function(icon) {
        // The 'icon' parameter can be either a Gio.Icon or a string.
        // I removed the Gio.Icon possibility because of issue #10
        /*if (typeof icon != 'string' && icon instanceof GObject.Object && GObject.type_is_a(icon, Gio.Icon))
            this._icon.gicon = icon;
        else
            this._icon.icon_name = icon;*/
        this._icon.icon_name = icon;
    }
});

