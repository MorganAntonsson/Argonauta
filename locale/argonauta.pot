msgid ""
msgstr ""
"Project-Id-Version: Argonauta 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-24-02 20:00-0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: LANGUAGE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# please use translations concise enough to prevent horizontal scrolling (some meaningless words can be ignored if necessary)

#prefs.js

msgid "Preferences"
msgstr ""

msgid "Shortcuts"
msgstr ""

msgid "About"
msgstr ""

msgid "Files view"
msgstr ""

msgid "Show hidden files"
msgstr ""

msgid "Fade-in animation when browsing folders"
msgstr ""

msgid "Animate icons when hovering"
msgstr ""

msgid "Only thumbnails"
msgstr ""

msgid "All"
msgstr ""

msgid "Icon size"
msgstr ""

msgid "Factor 1 is apps view icon size"
msgstr ""

msgid "Maximum number of columns"
msgstr ""

msgid "Minimum number of rows"
msgstr ""

msgid "Start view"
msgstr ""

msgid "Where I left"
msgstr ""

msgid "Recent"
msgstr ""

msgid "Folder"
msgstr ""

msgid "Favorites"
msgstr ""

msgid "Start directory"
msgstr ""

msgid "Browse"
msgstr ""

msgid "Select directory"
msgstr ""

msgid "Run launchers on clicked"
msgstr ""

msgid ".desktop files must be executable"
msgstr ""

msgid "Never"
msgstr ""

msgid "Only in"
msgstr ""

msgid "Everywhere"
msgstr ""

msgid "Toggle to Files view"
msgstr ""

msgid "Add toggle button in panel"
msgstr ""

msgid "right panel is status area"
msgstr ""

msgid "None"
msgstr ""

msgid "To the left"
msgstr ""

msgid "To the center"
msgstr ""

msgid "To the right"
msgstr ""

msgid "Panel button accurate position"
msgstr ""

msgid "0 for left, -1 for right"
msgstr ""

msgid "Panel button appearance"
msgstr ""

msgid "Icon"
msgstr ""

msgid "Text"
msgstr ""

msgid "Add toggle button in dash/dock"
msgstr ""

msgid "Above"
msgstr ""

msgid "Below"
msgstr ""

msgid "Icon used"
msgstr ""

msgid "default"
msgstr ""

msgid "Visualize icons and their name"
msgstr ""

msgid "need gtk-3-examples package"
msgstr ""

msgid "Open"
msgstr ""

msgid "Add a hot corner"
msgstr ""

msgid "Top right"
msgstr ""

msgid "Bottom left"
msgstr ""

msgid "Bottom right"
msgstr ""

msgid "Shortcut to display Files view"
msgstr ""

#shortcuts

msgid "Internal shortcuts"
msgstr ""

msgid "not editable"
msgstr ""

msgid "Show/hide hidden files"
msgstr ""

msgid "Open location dialog"
msgstr ""

msgid "apply completion"
msgstr ""

msgid "browse history"
msgstr ""

msgid "Go to parent folder"
msgstr ""

msgid "Go to home folder"
msgstr ""

msgid "Go to previous folder"
msgstr ""

msgid "Switch to left tab"
msgstr ""

msgid "Switch to right tab"
msgstr ""

msgid "Increase max-columns number"
msgstr ""

msgid "Decrease max-columns number"
msgstr ""

msgid "Increase min-rows number"
msgstr ""

msgid "Decrease min-rows number"
msgstr ""

msgid "Show/hide dash in overview"
msgstr ""

msgid "Refresh view"
msgstr ""

#about

msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
""

msgid "A files-view for GNOME Shell.\n\nThis extension displays a 'Files' view similar to the Applications view, whose design is taken from it (same layout, same style classes). It is accessed through the combination `SUPER + F`"
msgstr ""

msgid "Version"
msgstr ""

msgid "Credits"
msgstr ""

#extension.js

msgid "Files"
msgstr ""

#filesDisplay.js

#msgid "Recent" //already in prefs
#msgstr ""

msgid "Recent Files"
msgstr ""

#msgid "Favorites" //already in prefs
#msgstr ""

msgid "Favorite Files"
msgstr ""

msgid "No permission to open"
msgstr ""

msgid "no longer exists and was removed from favorites"
msgstr ""

#fileIcon.js

msgid "Open with ..."
msgstr ""

msgid "No applications for this file"
msgstr ""

msgid "Set as wallpaper"
msgstr ""

msgid "Remove as wallpaper"
msgstr ""

msgid "Run"
msgstr ""

msgid "Run in a terminal"
msgstr ""

msgid "Run in the background"
msgstr ""

msgid "Open the folder in Nautilus"
msgstr ""

msgid "Open the folder in Gnome-terminal"
msgstr ""

msgid "Copy path to clipboard"
msgstr ""

msgid "Add to favorites"
msgstr ""

msgid "Remove favorites"
msgstr ""

#buttons.js

#msgid "Files" //already in extension.js
#msgstr ""

msgid "Show Files"
msgstr ""

msgid "Files View settings"
msgstr ""

#pathDialog.js

msgid "Enter location"
msgstr ""

msgid "File or folder not found"
msgstr ""
