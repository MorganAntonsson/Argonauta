��    ^           �      �  !   �       �   4  �   	     �	     �	     �	      
     
     0
     K
     O
     k
     q
     }
     �
     �
     �
     �
     �
     �
  
   �
     �
  '        G  	   V     `     y       
   �     �     �     �     �     �  	   �  	   �     �          /     B     \     s     y     �     �     �     �     �     �  !   �          "     0     O     g     s     z     �     �     �     �     �     �     �     �             	   ?  
   I     T     f     �     �  
   �     �     �     �     �     �     �       	        %     -     L     Y     j     y     �  /   �     �     �  /  �  0   %  #   V  �   z  �   f  	   b  	   l     v     �  2   �  0   �          	  
   &     1     B  	   S  &   ]     �  &   �  $   �     �     �  >   �  :   5     p     �     �     �     �     �     �     �     �           7     >     Q  '   b  %   �     �     �     �     �  "   �     "     6     <     K     c  8   j  %   �     �     �     �          #     1     :     L     ^     y  	   �     �     �  "   �     �       '   )  
   Q     \     r  .   �  $   �     �     �            (     I  	   O  	   Y  	   c     m     �     �  "   �     �     �     �       !   !  ,   C     p  2   �                     Z   M   /             6               G       U            E           5   L           =      J   *   X   2       #           >       ;       Y   V                 "             ^   W   +   O   Q   D      	   8          B       \   C   [          ]   <   -   3      H   (      F   S   0   .   A   P       R      T      $          I                    1       7   9      :       @   &   4   N          !   
       ?   %   K   ,   '          )                    .desktop files must be executable 0 for left, -1 for right <span size="small">This program comes with ABSOLUTELY NO WARRANTY.
See the <a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License, version 2 or later</a> for details.</span> A files-view for GNOME Shell.

This extension displays a 'Files' view similar to the Applications view, whose design is taken from it (same layout, same style classes). It is accessed through the combination `SUPER + F` About Above Add a hot corner Add to favorites Add toggle button in dash/dock Add toggle button in panel All Animate icons when hovering Below Bottom left Bottom right Browse Copy path to clipboard Credits Decrease max-columns number Decrease min-rows number Enter location Everywhere Factor 1 is apps view icon size Fade-in animation when browsing folders Favorite Files Favorites File or folder not found Files Files View settings Files view Folder Go to home folder Go to parent folder Go to previous folder Icon Icon size Icon used Increase max-columns number Increase min-rows number Internal shortcuts Maximum number of columns Minimum number of rows Never No applications for this file No permission to open None Only in Only thumbnails Open Open location dialog Open the folder in Gnome-terminal Open the folder in Nautilus Open with ... Panel button accurate position Panel button appearance Preferences Recent Recent Files Refresh view Remove as wallpaper Remove favorites Run Run in a terminal Run in the background Run launchers on clicked Select directory Set as wallpaper Shortcut to display Files view Shortcuts Show Files Show hidden files Show/hide dash in overview Show/hide hidden files Start directory Start view Switch to left tab Switch to right tab Text To the center To the left To the right Toggle to Files view Top right Version Visualize icons and their name Where I left apply completion browse history default need gtk-3-examples package no longer exists and was removed from favorites not editable right panel is status area Project-Id-Version: Argonauta 0.1
Report-Msgid-Bugs-To: 
POT-Revision-Date: 2018-01-04 00:00-0100
Last-Translator: Abakkk <argonauta@framagit.org>
Language-Team: Français <argonauta@framagit.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Les fichiers .desktop doivent être exécutables 0 pour la gauche, -1 pour la droite <span size="small">Ce programme est fourni SANS AUCUNE GARANTIE.
Pour plus de détails, visitez la <a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html">Licence publique générale GNU, version 2 ou ultérieure</a></span> Un visionneur de fichiers pour GNOME Shell.

Cette extension affiche une vue 'Fichiers' comparable à la vue Applications, dont elle reprend le design (même disposition, mêmes classes de style). On y accède grâce à la combinaison `SUPER + F`. À propos Au dessus Ajouter un coin actif Ajouter aux favoris Ajouter un bouton de basculement dans le dash/dock Ajouter un bouton de basculement dans le panneau Tous Animer les icônes au survol En dessous En bas à gauche En bas à droite Parcourir Copier le chemin dans le presse-papier Crédits Diminuer le nombre maximum de colonnes Diminuer le nombre minimum de lignes Saisir l'emplacement Partout 1 correspond à la taille des icônes dans la vue applications Animation en fondu lors de la navigation dans les fichiers Fichiers favoris Favoris Fichier ou dossier non trouvé Fichiers Paramètres de Files View La vue Fichiers Dossier Aller au dossier personnel Aller au dossier parent Retourner au dossier précédent Icône Taille des icônes Icône utilisée Augmenter le nombre maximum de colonnes Augmenter le nombre minimum de lignes Raccourcis internes Nombre maximum de colonnes Nombre minimum de lignes Jamais Aucune application pour ce fichier Impossible d'ouvrir Aucun Seulement dans Seulement les vignettes Ouvrir Ouvrir la fenêtre de dialogue pour saisir l'emplacement Ouvrir le dossier dans Gnome-terminal Ouvrir le dossier dans Nautilus Ouvrir avec Position exacte du bouton Apparence du bouton Préférences Récents Fichiers récents Actualiser la vue Retirer comme papier peint Retirer des favoris Exécuter Exécuter dans un terminal Exécuter en arrière-plan Exécuter les lanceurs en cliquant Sélectionner un répertoire Définir comme papier peint Raccourci pour afficher la vue Fichiers Raccourcis Afficher les fichiers Montrer les fichiers cachés Montrer/masquer le dash dans la vue d'ensemble Montrer/masquer les fichiers cachés Répertoire d'accueil Vue d'accueil Basculer vers l'onglet de gauche Basculer vers l'onglet de droite Texte Au centre À gauche À droite Basculer vers la vue Fichiers En haut à droite Version Visualiser les icônes et leur nom Là où j'ai quitté appliquer l'auto-complétion naviguer dans l'historique par défaut requiert le paquet gtk-3-examples n'existe plus et a été retiré des favoris non modifiables la partie droite du panneau est la zone de statuts 