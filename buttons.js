/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Clutter = imports.gi.Clutter;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const St = imports.gi.St;

const Dash = imports.ui.dash;
const Main = imports.ui.main;
const Mainloop = imports.mainloop;
const PopupMenu = imports.ui.popupMenu;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Util = imports.misc.util;
const Convenience = Extension.imports.convenience;
const FileIcon = Extension.imports.fileIcon;

const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;


var PanelIcon = new Lang.Class({
    Name: 'PanelIcon',
    
    _init: function() {
        this.settings = Convenience.getSettings();
        this.button = new St.Bin({ name: 'toggle-files-view', style_class: 'panel-button', reactive: true, can_focus: true, x_fill: true, y_fill: false, track_hover: true });
        if (Convenience.getSettings().get_string('panel-button-appearance') == 'icon')
            this.buttonContent = new St.Icon({ icon_name: this.settings.get_string('icon-name'), style_class: 'system-status-icon' });
        else
            this.buttonContent = new St.Label({ text: _("Files"), y_expand: true, y_align: Clutter.ActorAlign.CENTER });
        this.button.set_child(this.buttonContent);
        let side = this.settings.get_string('panel-button');
        if (side == 'left')
            this.box = Main.panel._leftBox;
        else if (side == 'center')
            this.box = Main.panel._centerBox;
        else
            this.box = Main.panel._rightBox;
        this.box.insert_child_at_index(this.button, this.settings.get_int('panel-button-position'));
    },
    
    disable: function() {
        this.box.remove_child(this.button);
        this.button.destroy();
    }
});


var DashIcon = new Lang.Class({
    Name: 'DashIcon',
    
    _init: function() {
        let dash = Main.overview._dash;
        // _dtpSettings is a Dash-to-Panel marker
        // Dash-to-Panel appropriate Main.overview._dash but original dash is still available in Main.overview._controls.dash
        if (dash._dtpSettings)
            dash = Main.overview._controls.dash;
        this.dash = dash;
        
        let dashIsDock = dash._scrollView ? true : false;
        if (!dashIsDock) {
            this._adjustIconSizeOld = _adjustIconSize;
            dash._adjustIconSize = _adjustIconSize;
        }
        this.showFilesIcon = new DashShowFilesIcon();
        
        //tooltip
        this.showFilesIcon.setLabelText(_("Show Files"));
        dash._hookUpLabel(this.showFilesIcon);
        
        this.actor = this.showFilesIcon.toggleButton;
        this.showFilesIcon.childScale = 1;
        this.showFilesIcon.childOpacity = 255;
        this.showFilesIcon.icon.setIconSize(dash.iconSize*0.85);
        
        dash._showFilesIcon = this.showFilesIcon;
        dash.showFilesButton = this.showFilesIcon.toggleButton; // needed to animate filesPage
        dash._container.remove_actor(dash._showAppsIcon);
        this.boxLayout = new St.BoxLayout({ vertical: true, clip_to_allocation: true });
        
        if (Convenience.getSettings().get_string('dash-button') == 'above') {
            this.boxLayout.add_actor(this.showFilesIcon);
            this.boxLayout.add_actor(dash._showAppsIcon);
        } else {
            this.boxLayout.add_actor(dash._showAppsIcon);
            this.boxLayout.add_actor(this.showFilesIcon);
        }
        
        dash._container.add_actor(this.boxLayout);
        this.boxLayout.set_style('padding-top:4px;spacing:0px');
        
        if (dashIsDock)
            dash.actor.set_width(this.boxLayout.get_width());
        
        this.iconSizeChangedHandler = dash.connect('icon-size-changed', () => {
            this.showFilesIcon.icon.setIconSize(dash.iconSize*0.85);
            if (dashIsDock)
                this.iconSizeTimeout = Mainloop.timeout_add(200, () => {
                    dash.actor.set_width(this.boxLayout.get_width());
                    Mainloop.source_remove(this.iconSizeTimeout);
                    this.iconSizeTimeout = null;
                });
        });
        
        this.actor.connect('button-press-event', Lang.bind(this, this._onButtonPress));
        this.actor.connect('popup-menu', Lang.bind(this, this._onKeyboardPopupMenu));
        
        this._menu = null;
        this._menuManager = new PopupMenu.PopupMenuManager(this);
    },
    
    _onButtonPress: function(actor, event) {
        let button = event.get_button();
        if (button == 3) {
            this.popupMenu();
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    },
    
    _onKeyboardPopupMenu: function() {
        this.popupMenu();
        this._menu.actor.navigate_focus(null, Gtk.DirectionType.TAB_FORWARD, false);
    },
    
    _onMenuPoppedDown: function() {
		this.actor.sync_hover();
	},
    
    popupMenu: function() {
        if (!this._menu) {
            this._menu = new DashShowFilesIconMenu(this);
            this._menu.connect('open-state-changed', Lang.bind(this, function (menu, isPoppedUp) {
				if (!isPoppedUp) this._onMenuPoppedDown();
			}));
			this._menuManager.addMenu(this._menu);
        }
        
		this.actor.set_hover(true);
		this._menu.popup();
		this._menuManager.ignoreRelease();
		return false;
    },
    
    disable: function() {
        let dash = this.dash;
        if (this.iconSizeChangedHandler)
            dash.disconnect(this.iconSizeChangedHandler);
        if (this.iconSizeTimeout)
            Mainloop.source_remove(this.iconSizeTimeout);
        if (this._adjustIconSizeOld)
            dash._adjustIconSize = this._adjustIconSizeOld;
        this.boxLayout.remove_actor(dash._showAppsIcon);
        dash._container.remove_actor(this.boxLayout);
        dash._container.add_actor(dash._showAppsIcon);
        dash.actor.set_width(-1);
        this.showFilesIcon.destroy();
        dash.showFilesButton = null;
    }
});

var DashShowFilesIcon = new Lang.Class({
    Name: 'ShowFilesIcon',
    Extends: Dash.ShowAppsIcon,

    _createIcon: function(size) {
        this._iconActor = new St.Icon({ icon_name: Convenience.getSettings().get_string('icon-name'),
                                        icon_size: size,
                                        style_class: 'show-apps-icon',
                                        track_hover: true });
        return this._iconActor;
    }
});

var DashShowFilesIconMenu = new Lang.Class({
    Name: 'FilesIconMenu',
    Extends: FileIcon.FileIconMenu,
    
    _redisplay: function() {
        this.removeAll();
        let openPrefItem = new FileIcon.PopupImageMenuItemFixed(_("Files View settings"), 'emblem-system-symbolic');
        this.addMenuItem(openPrefItem);
        openPrefItem.connect('activate', Lang.bind(this, function() {
                                        Main.overview.hide();
                                        Util.spawn(["gnome-shell-extension-prefs", Extension.metadata.uuid]);
                                     }));
    }
});

function _adjustIconSize() {
    const baseIconSizes = [ 16, 22, 24, 32, 48, 64 ];
    const DASH_ANIMATION_TIME = 0.2;
    const Tweener = imports.ui.tweener;
    // For the icon size, we only consider children which are "proper"
    // icons (i.e. ignoring drag placeholders) and which are not
    // animating out (which means they will be destroyed at the end of
    // the animation)
    let iconChildren = this._box.get_children().filter(actor => {
        return actor.child &&
               actor.child._delegate &&
               actor.child._delegate.icon &&
               !actor.animatingOut;
    });

    iconChildren.push(this._showAppsIcon);

    if (this._maxHeight == -1)
        return;

    let themeNode = this._container.get_theme_node();
    let maxAllocation = new Clutter.ActorBox({ x1: 0, y1: 0,
                                               x2: 42 /* whatever */,
                                               y2: this._maxHeight });
    let maxContent = themeNode.get_content_box(maxAllocation);
    let availHeight = maxContent.y2 - maxContent.y1;
    let spacing = themeNode.get_length('spacing');

    let firstButton = iconChildren[0].child;
    let firstIcon = firstButton._delegate.icon;

    let minHeight, natHeight;
    let scaleFactor = St.ThemeContext.get_for_stage(global.stage).scale_factor;

    // Enforce the current icon size during the size request
    firstIcon.icon.ensure_style();
    let [currentWidth, currentHeight] = firstIcon.icon.get_size();
    firstIcon.icon.set_size(this.iconSize * scaleFactor, this.iconSize * scaleFactor);
    [minHeight, natHeight] = firstButton.get_preferred_height(-1);
    firstIcon.icon.set_size(currentWidth, currentHeight);

    // Subtract icon padding and box spacing from the available height
    availHeight -= (iconChildren.length + 2) * (natHeight - this.iconSize * scaleFactor) +
                   (iconChildren.length + 1) * spacing;

    let availSize = availHeight / iconChildren.length;

    let iconSizes = baseIconSizes.map(s => s * scaleFactor);

    let newIconSize = baseIconSizes[0];
    for (let i = 0; i < iconSizes.length; i++) {
        if (iconSizes[i] < availSize)
            newIconSize = baseIconSizes[i];
    }

    if (newIconSize == this.iconSize)
        return;

    let oldIconSize = this.iconSize;
    this.iconSize = newIconSize;
    this.emit('icon-size-changed');

    let scale = oldIconSize / newIconSize;
    for (let i = 0; i < iconChildren.length; i++) {
        let icon = iconChildren[i].child._delegate.icon;

        // Set the new size immediately, to keep the icons' sizes
        // in sync with this.iconSize
        icon.setIconSize(this.iconSize);

        // Don't animate the icon size change when the overview
        // is transitioning, not visible or when initially filling
        // the dash
        if (!Main.overview.visible || Main.overview.animationInProgress ||
            !this._shownInitially)
            continue;

        let [targetWidth, targetHeight] = icon.icon.get_size();

        // Scale the icon's texture to the previous size and
        // tween to the new size
        icon.icon.set_size(icon.icon.width * scale,
                           icon.icon.height * scale);

        Tweener.addTween(icon.icon,
                         { width: targetWidth,
                           height: targetHeight,
                           time: DASH_ANIMATION_TIME,
                           transition: 'easeOutQuad',
                         });
    }
}

