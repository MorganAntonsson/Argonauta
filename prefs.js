/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;

const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const Metadata = Extension.metadata;

const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;

const MARGIN = 10;


function init() {
    Convenience.initTranslations();
}


function buildPrefsWidget() {
    let widget = new PrefsWidget();
    let switcher = new Gtk.StackSwitcher({halign: Gtk.Align.CENTER, visible: true, stack: widget});
    Mainloop.timeout_add(0, () => {
        let headerBar = widget.get_toplevel().get_titlebar();
        headerBar.custom_title = switcher;
        return false;
    });
    
    widget.show_all();

    return widget;
}

const PrefsWidget = new GObject.Class({
    Name: 'Prefs.Widget',
    GTypeName: 'PrefsWidget',
    Extends: Gtk.Stack,
    
    _init: function(params) {
        this.parent({ transition_type: 1, transition_duration: 500 });
        let page1 = new FilesPrefsPage();
        this.add_titled(page1, 'prefs', _("Preferences"));
        let page2 = new ShortcutsPage();
        this.add_titled(page2, 'shortcuts', _("Shortcuts"));
        let page3 = new AboutPage();
        this.add_titled(page3, 'about', _("About"));
    }
});


const AboutPage = new GObject.Class({
    Name: 'About.Page',
    GTypeName: 'AboutPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent({vexpand: false});
        this.set_min_content_height(600);
        //this.set_max_content_height(600); unsupported before 3.22

        this.settings = Convenience.getSettings();
        
        let vbox= new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL, margin: MARGIN*3 });
        this.add(vbox);
        
        
        let imageBox = new Gtk.Box();
        let png = Extension.dir.get_child('png').get_child('files-view.png').get_path();
        let image = new Gtk.Image({ file: png, pixel_size: 96 });
        imageBox.set_center_widget(image);
        vbox.pack_start(imageBox, false, false, 0);
        
        let licence = _("<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\nSee the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">GNU General Public License, version 2 or later</a> for details.</span>");
        
        let textBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let text = new Gtk.Label({ wrap: true, justify: 2, use_markup: true,
                                  label: "<big><b>" + Metadata.name + "</b></big>" + "\n" +
                                         "<small>Version" + " " + Metadata.version +"</small>\n\n" +
                                         _(Metadata.description) + "\n\n" +
                                         "<span><a href=\"" + Metadata.url + "\">" + Metadata.url + "</a></span>" + "\n\n" +
                                         licence + "\n" });
        textBox.pack_start(text, false, false, 0);
        
        let creditBox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });
        let leftBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let rightBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let leftLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 2, justify: 1, use_markup: true, label: "<small><u>" + _("Credits") + ":</u></small>" });
        let rightLabel = new Gtk.Label({ wrap: true, valign: 1, halign: 1, justify: 0, use_markup: true, label: "<small>Abakkk" + 
                                            "\nSam Bull, <span><a href=\"https://extensions.gnome.org/extension/568/notes\">Sticky Notes View</a></span>" +
                                            "\nEccheng, <span><a href=\"https://extensions.gnome.org/extension/1037/customcorner\">CustomCorner</a></span>" +
                                            "\nAuthors of <span><a href=\"https://wiki.gnome.org/Projects/GnomeShell\">GNOME Shell</a></span></small>" });
        leftBox.pack_start(leftLabel, true, true, 0);
        rightBox.pack_start(rightLabel, true, true, 0);
        creditBox.pack_start(leftBox, true, true, 5);
        creditBox.pack_start(rightBox, true, true, 5);
        textBox.pack_start(creditBox, false, false, 0);
        
        vbox.pack_start(textBox, false, false, 0);
    }
    
});


const FilesPrefsPage = new GObject.Class({
    Name: 'Files.Prefs.Page',
    GTypeName: 'FilesPrefsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();
        this.set_min_content_height(600);
        //this.set_max_content_height(600); unsupported before 3.22

        this.settings = Convenience.getSettings();
        
        let box = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
        this.add(box);
        
        let filesLabel = new Gtk.Label({ use_markup: true, label: "<b>" + _("Files view") + "</b>" });
        let filesFrame = new Gtk.Frame({ valign: 1, shadow_type: 0,
                                    label_widget: filesLabel, label_yalign: 0.5, label_xalign: 0.5,
                                    margin: MARGIN*2 });
        box.add(filesFrame);
        let filesListBox = new Gtk.ListBox({ selection_mode: 0, margin: MARGIN });
        filesFrame.add(filesListBox);
        
            
        let hiddenBox = new Gtk.Box({ margin: MARGIN });
        let hiddenLabel = new Gtk.Label({label: _("Show hidden files")});
        hiddenLabel.set_halign(1);
        let hiddenSwitch = new Gtk.Switch({valign: 3});
        this.settings.bind("show-hidden-files", hiddenSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        hiddenBox.pack_start(hiddenLabel, true, true, 4);
        hiddenBox.pack_start(hiddenSwitch, false, false, 4);
        filesListBox.add(hiddenBox);
        this.addSeparator(filesListBox);
        
        
        let fadeBox = new Gtk.Box({ margin: MARGIN });
        let fadeLabel = new Gtk.Label({label: _("Fade-in animation when browsing folders")});
        fadeLabel.set_halign(1);
        let fadeSwitch = new Gtk.Switch({valign: 3});
        this.settings.bind("fade-in-browsing", fadeSwitch, "active", Gio.SettingsBindFlags.DEFAULT);
        fadeBox.pack_start(fadeLabel, true, true, 4);
        fadeBox.pack_start(fadeSwitch, false, false, 4);
        filesListBox.add(fadeBox);
        
        
        let hoverBox = new Gtk.Box({ margin: MARGIN });
        let hoverLabel = new Gtk.Label({ label: _("Animate icons when hovering") });
        hoverLabel.set_halign(1);
        let hoverCombo = new Gtk.ComboBoxText({valign: 3});
        hoverCombo.append('none',_("None"));
        hoverCombo.append('thumbnails',_("Only thumbnails"));
        hoverCombo.append('all',_("All"));
        this.settings.bind("icons-hover", hoverCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        hoverBox.pack_start(hoverLabel, true, true, 4);
        hoverBox.pack_start(hoverCombo, false, false, 4);
        filesListBox.add(hoverBox);
        this.addSeparator(filesListBox);
        
        
        let viewIconSizeBox = new Gtk.Box({ margin: MARGIN });
        let viewIconSizeLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let viewIconSizeLabel1 = new Gtk.Label({ halign: 1, label: _("Icon size") });
        let viewIconSizeLabel2 = new Gtk.Label({ use_markup: true, halign: 1, label: "<small>" + _("Factor 1 is apps view icon size") + "</small>" });
        viewIconSizeLabel2.get_style_context().add_class("dim-label");
        viewIconSizeLabelBox.pack_start(viewIconSizeLabel1, true, true, 0);
        viewIconSizeLabelBox.pack_start(viewIconSizeLabel2, true, true, 0);
        let viewIconSizeSpin = Gtk.SpinButton.new_with_range(0.5,1.5,0.05);
        viewIconSizeSpin.set_valign(1);
        this.settings.bind("view-icon-size-factor", viewIconSizeSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        viewIconSizeBox.pack_start(viewIconSizeLabelBox, true, true, 4);
        viewIconSizeBox.pack_start(viewIconSizeSpin, false, false, 4);
        filesListBox.add(viewIconSizeBox);
        
        
        let columnsBox = new Gtk.Box({ margin: MARGIN });
        let columnsLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let columnsLabel1 = new Gtk.Label({ halign: 1, label: _("Maximum number of columns") });
        let columnsLabel2 = new Gtk.Label({ use_markup: true, halign: 1, label: "<small>" + _("default") + " 6</small>" });
        columnsLabel2.get_style_context().add_class("dim-label");
        columnsLabelBox.pack_start(columnsLabel1, true, true, 0);
        columnsLabelBox.pack_start(columnsLabel2, true, true, 0);
        let columnsSpin = Gtk.SpinButton.new_with_range(1,14,1);
        columnsSpin.set_valign(1);
        this.settings.bind("max-columns", columnsSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        columnsBox.pack_start(columnsLabelBox, true, true, 4);
        columnsBox.pack_start(columnsSpin, false, false, 4);
        filesListBox.add(columnsBox);
        
        
        let rowsBox = new Gtk.Box({ margin: MARGIN });
        let rowsLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let rowsLabel1 = new Gtk.Label({ halign: 1, label: _("Minimum number of rows") });
        let rowsLabel2 = new Gtk.Label({ use_markup: true, halign: 1, label: "<small>" + _("default") + " 4</small>" });
        rowsLabel2.get_style_context().add_class("dim-label");
        rowsLabelBox.pack_start(rowsLabel1, true, true, 0);
        rowsLabelBox.pack_start(rowsLabel2, true, true, 0);
        let rowsSpin = Gtk.SpinButton.new_with_range(2,12,1);
        rowsSpin.set_valign(1);
        this.settings.bind("min-rows", rowsSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        rowsBox.pack_start(rowsLabelBox, true, true, 4);
        rowsBox.pack_start(rowsSpin, false, false, 4);
        filesListBox.add(rowsBox);
        this.addSeparator(filesListBox);
        
        
        let viewBox = new Gtk.Box({ margin: MARGIN });
        let viewLabel = new Gtk.Label({ label: _("Start view") });
        viewLabel.set_halign(1);
        let viewCombo = new Gtk.ComboBoxText({ valign: 3 });
        viewCombo.append('previous',_("Where I left"));
        viewCombo.append('recent',_("Recent"));
        viewCombo.append('folder',_("Folder"));
        viewCombo.append('favorites',_("Favorites"));
        this.settings.bind("start-view", viewCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        viewBox.pack_start(viewLabel, true, true, 4);
        viewBox.pack_start(viewCombo, false, false, 4);
        filesListBox.add(viewBox);
        
        
        let startBox = new Gtk.Box({ margin: MARGIN });
        let startLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let startLabel1 = new Gtk.Label({ xalign: 0, label: _("Start directory") });
        startLabelBox.pack_start(startLabel1, true, true, 0);
        let startEntry = new Gtk.Entry({ hexpand: true, valign: 1 });
        this.startEntry = startEntry;
        this.settings.bind("start-path", startEntry, "text", Gio.SettingsBindFlags.DEFAULT);
        let startButton = new Gtk.Button({ label: _("Browse"), valign: 1 });
        startButton.connect('clicked', Lang.bind(this, this._launchFileChooser));
        startBox.pack_start(startLabelBox, true, true, 4);
        startBox.pack_start(startEntry, true, true, 4);
        startBox.pack_start(startButton, false, false, 4);;
        filesListBox.add(startBox);
        this.addSeparator(filesListBox);        
        
        
        let launcherBox = new Gtk.Box({ margin: MARGIN });
        let launcherLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let launcherLabel1 = new Gtk.Label({ label: _("Run launchers on clicked") });
        let launcherLabel2 = new Gtk.Label({ use_markup: true, halign: 1, label: "<small>" + _(".desktop files must be executable") + "</small>" });
        launcherLabel1.set_halign(1);
        launcherLabel2.get_style_context().add_class("dim-label");
        launcherLabelBox.pack_start(launcherLabel1, true, true, 0);
        launcherLabelBox.pack_start(launcherLabel2, true, true, 0);
        let launcherCombo = new Gtk.ComboBoxText({valign: 3});
        let desktopPath = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP);
        let shortDesktopPath = ".../" + desktopPath.split('/')[desktopPath.split('/').length - 1];
        launcherCombo.append('none',_("Never"));
        launcherCombo.append('desktop-dir',_("Only in") + " " + shortDesktopPath);
        launcherCombo.append('everywhere',_("Everywhere"));
        this.settings.bind("run-launchers", launcherCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        launcherBox.pack_start(launcherLabelBox, true, true, 4);
        launcherBox.pack_start(launcherCombo, false, false, 4);
        filesListBox.add(launcherBox);
        this.addSeparator(filesListBox);
        
        
        let displayLabel = new Gtk.Label({ use_markup: true, label: "<b>" + _("Toggle to Files view") + "</b>" });
        let displayFrame = new Gtk.Frame({ shadow_type: 0,
                                    label_widget: displayLabel, label_yalign: 0.5, label_xalign: 0.5,
                                    margin: MARGIN*2 });
        box.add(displayFrame);
        let displayListBox = new Gtk.ListBox({ selection_mode: 0, margin: MARGIN }); // 0 is 'none'
        displayFrame.add(displayListBox);
        
        
        let panelBox = new Gtk.Box({ margin: MARGIN });
        let panelLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let panelLabel1 = new Gtk.Label({ halign: 1, label: _("Add toggle button in panel") });
        let panelLabel2 = new Gtk.Label({ halign: 1, use_markup: true, label: "<small>" + _("right panel is status area") + "</small>" });
        panelLabel2.get_style_context().add_class("dim-label");
        panelLabelBox.pack_start(panelLabel1, true, true, 0);
        panelLabelBox.pack_start(panelLabel2, true, true, 0);
        let panelCombo = new Gtk.ComboBoxText({ valign: 1 });
        panelCombo.append('none',_("None"));
        panelCombo.append('left',_("To the left"));
        panelCombo.append('center',_("To the center"));
        panelCombo.append('right',_("To the right"));
        this.settings.bind("panel-button", panelCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        panelBox.pack_start(panelLabelBox, true, true, 4);
        panelBox.pack_start(panelCombo, false, false, 4);
        displayListBox.add(panelBox);
        
        
        let panelPositionBox = new Gtk.Box({ margin: MARGIN });
        let panelPositionLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let panelPositionLabel1 = new Gtk.Label({ halign: 1, label: _("Panel button accurate position") });
        let panelPositionLabel2 = new Gtk.Label({ halign: 1, use_markup: true, label: "<small>" + _("0 for left, -1 for right") + "</small>" });
        panelPositionLabel2.get_style_context().add_class("dim-label");
        panelPositionLabelBox.pack_start(panelPositionLabel1, true, true, 0);
        panelPositionLabelBox.pack_start(panelPositionLabel2, true, true, 0);
        let panelPositionSpin = Gtk.SpinButton.new_with_range(-1,99,1);
        panelPositionSpin.set_valign(1);
        this.settings.bind("panel-button-position", panelPositionSpin, "value", Gio.SettingsBindFlags.DEFAULT);
        panelPositionBox.pack_start(panelPositionLabelBox, true, true, 4);
        panelPositionBox.pack_start(panelPositionSpin, false, false, 4);
        displayListBox.add(panelPositionBox);
        
        
        let panelAppearanceBox = new Gtk.Box({ margin: MARGIN });
        let panelAppearanceLabel = new Gtk.Label({ halign: 1, label: _("Panel button appearance") });
        let radioBox = new Gtk.Box({ orientation: Gtk.Orientation.HORIZONTAL });
        let iconRadio = new Gtk.RadioButton({ label: _("Icon") });
        iconRadio.connect('toggled', (widget) => {
            if (widget.get_active())
                this.settings.set_string('panel-button-appearance', 'icon');
        });
        let textRadio = new Gtk.RadioButton({ group: iconRadio, label: _("Text") });
        textRadio.connect('toggled', (widget) => {
            if (widget.get_active())
                this.settings.set_string('panel-button-appearance', 'text');
        });
        if (this.settings.get_string('panel-button-appearance') == 'icon')
            iconRadio.set_active(true);
        else
            textRadio.set_active(true);
        radioBox.pack_start(iconRadio, true, true, 4);
        radioBox.pack_start(textRadio, true, true, 4);
        panelAppearanceBox.pack_start(panelAppearanceLabel, true, true, 4);
        panelAppearanceBox.pack_start(radioBox, false, false, 0);
        displayListBox.add(panelAppearanceBox);
        this.addSeparator(displayListBox);
        
        
        let dashBox = new Gtk.Box({ margin: MARGIN });
        let dashLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let dashLabel1 = new Gtk.Label({ halign: 1, label: _("Add toggle button in dash/dock") });
        dashLabelBox.pack_start(dashLabel1, true, true, 0);
        let dashCombo = new Gtk.ComboBoxText({ valign: 1 });
        dashCombo.append('none',_("None"));
        dashCombo.append('above',_("Above"));
        dashCombo.append('below',_("Below"));
        this.settings.bind("dash-button", dashCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        dashBox.pack_start(dashLabelBox, true, true, 4);
        dashBox.pack_start(dashCombo, false, false, 4);
        displayListBox.add(dashBox);
        
        
        let iconBox = new Gtk.Box({ margin: MARGIN });
        let iconLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let iconLabel1 = new Gtk.Label({ halign: 1, label: _("Icon used") });
        let iconLabel2 = new Gtk.Label({ halign: 1, use_markup: true, label: "<small>" + _("default") + " <i>folder-symbolic</i>" + "</small>" });
        iconLabel2.get_style_context().add_class("dim-label");
        iconLabelBox.pack_start(iconLabel1, true, true, 0);
        iconLabelBox.pack_start(iconLabel2, true, true, 0);
        let iconEntry = new Gtk.Entry({ hexpand: true, valign: 1 });
        iconEntry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, this.settings.get_string('icon-name'));
        this.settings.bind("icon-name", iconEntry, "text", Gio.SettingsBindFlags.DEFAULT);
        iconBox.pack_start(iconLabelBox, true, true, 4);
        iconBox.pack_start(iconEntry, true, true, 4);
        displayListBox.add(iconBox);
        
        
        let visuBox = new Gtk.Box({ margin: MARGIN });
        let visuLabelBox = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
        let visuLabel1 = new Gtk.Label({ halign: 1, label: _("Visualize icons and their name") });
        let visuLabel2 = new Gtk.Label({ halign: 1, use_markup: true, label: "<small>" + _("need gtk-3-examples package") + "</small>" });
        visuLabel2.get_style_context().add_class("dim-label");
        visuLabelBox.pack_start(visuLabel1, true, true, 0);
        visuLabelBox.pack_start(visuLabel2, true, true, 0);
        let visuButton = new Gtk.Button({ label: _("Open"), valign: 1 });
        visuButton.connect('clicked', function() {
            GLib.spawn_command_line_sync("gtk3-icon-browser");
        });
        visuBox.pack_start(visuLabelBox, true, true, 4);
        visuBox.pack_start(visuButton, false, false, 4);
        displayListBox.add(visuBox);
        this.addSeparator(displayListBox);
        
        
        let cornerBox = new Gtk.Box({ margin: MARGIN });
        let cornerLabel = new Gtk.Label({ label: _("Add a hot corner") });
        cornerLabel.set_halign(1);
        let cornerCombo = new Gtk.ComboBoxText({ valign: 3 });
        cornerCombo.append('none',_("None"));
        cornerCombo.append('top-right',_("Top right"));
        cornerCombo.append('bottom-left',_("Bottom left"));
        cornerCombo.append('bottom-right',_("Bottom right"));
        this.settings.bind("hot-corner", cornerCombo, "active-id", Gio.SettingsBindFlags.DEFAULT);
        cornerBox.pack_start(cornerLabel, true, true, 4);
        cornerBox.pack_start(cornerCombo, false, false, 4);
        displayListBox.add(cornerBox);
        this.addSeparator(displayListBox);
        
        
        let toggleFilesViewkeybindings = { 'toggle-files-view': _("Shortcut to display Files view") };
        let toggleFilesViewKeybindingsWidget = new KeybindingsWidget(toggleFilesViewkeybindings, this.settings);
        toggleFilesViewKeybindingsWidget.margin = MARGIN;
        displayListBox.add(toggleFilesViewKeybindingsWidget);

    },
    
    _launchFileChooser: function() {
        let dialogTitle = _('Select directory');
        let chooser = new Gtk.FileChooserDialog({
                title: dialogTitle,
                action: Gtk.FileChooserAction.SELECT_FOLDER,
                modal: true
            });

        chooser.add_button(Gtk.STOCK_CANCEL, 0);
        chooser.add_button(Gtk.STOCK_OPEN, 1);
        chooser.set_default_response(1);
        chooser.set_current_folder(Convenience.getSettings().get_string('start-path'));
        let filename = null;
        if (chooser.run() == 1) {
            filename = chooser.get_filename();
            if (filename) {
                this.startEntry.set_text(filename);
                
            }   
        }
        chooser.destroy();
    },
    
    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow({sensitive: false});
        separatorRow.add(new Gtk.Separator({ margin_left: MARGIN, margin_right: MARGIN}));
        container.add(separatorRow);
    }
});


/*this code comes from Sticky Notes View by Sam Bull, https://extensions.gnome.org/extension/568/notes/ */
const KeybindingsWidget = new GObject.Class({
    Name: 'Keybindings.Widget',
    GTypeName: 'KeybindingsWidget',
    Extends: Gtk.Box,

    _init: function(keybindings, settings) {
        this.parent();
        this.set_orientation(Gtk.Orientation.VERTICAL);

        this._keybindings = keybindings;
        this._settings = settings;

        this._columns = {
            NAME: 0,
            ACCEL_NAME: 1,
            MODS: 2,
            KEY: 3
        };

        this._store = new Gtk.ListStore();
        this._store.set_column_types([
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_INT,
            GObject.TYPE_INT
        ]);

        this._tree_view = new Gtk.TreeView({
            model: this._store,
            hexpand: false,
            vexpand: false
        });
        this._tree_view.set_activate_on_single_click(false);
        this._tree_view.get_selection().set_mode(Gtk.SelectionMode.SINGLE);

        let action_renderer = new Gtk.CellRendererText();
        let action_column = new Gtk.TreeViewColumn({
            title: _(""),
            expand: true,
        });
        action_column.pack_start(action_renderer, true);
        action_column.add_attribute(action_renderer, 'text', 1);
        this._tree_view.append_column(action_column);
               
        let keybinding_renderer = new Gtk.CellRendererAccel({
            editable: true,
            accel_mode: Gtk.CellRendererAccelMode.GTK
        });
        keybinding_renderer.connect('accel-edited',
            Lang.bind(this, function(renderer, iter, key, mods) {
                let value = Gtk.accelerator_name(key, mods);
                let [success, iterator ] =
                    this._store.get_iter_from_string(iter);

                if(!success) {
                    printerr(_("Can't change keybinding"));
                }

                let name = this._store.get_value(iterator, 0);

                this._store.set(
                    iterator,
                    [this._columns.MODS, this._columns.KEY],
                    [mods, key]
                );
                this._settings.set_strv(name, [value]);
            })
        );

        let keybinding_column = new Gtk.TreeViewColumn({
            title: _(""),
        });
        keybinding_column.pack_end(keybinding_renderer, false);
        keybinding_column.add_attribute(
            keybinding_renderer,
            'accel-mods',
            this._columns.MODS
        );
        keybinding_column.add_attribute(
            keybinding_renderer,
            'accel-key',
            this._columns.KEY
        );
        this._tree_view.append_column(keybinding_column);
        this._tree_view.columns_autosize();
        this._tree_view.set_headers_visible(false);

        this.add(this._tree_view);
        this.keybinding_column = keybinding_column;
        this.action_column = action_column;

        this._refresh();
    },

    _refresh: function() {
        this._store.clear();

        for(let settings_key in this._keybindings) {
            let [key, mods] = Gtk.accelerator_parse(
                this._settings.get_strv(settings_key)[0]
            );

            let iter = this._store.append();
            this._store.set(iter,
                [
                    this._columns.NAME,
                    this._columns.ACCEL_NAME,
                    this._columns.MODS,
                    this._columns.KEY
                ],
                [
                    settings_key,
                    this._keybindings[settings_key],
                    mods,
                    key
                ]
            );
        }
    }
});


const ShortcutsPage = new GObject.Class({
    Name: 'Shortcuts.Page',
    GTypeName: 'ShortcutsPage',
    Extends: Gtk.ScrolledWindow,

    _init: function(params) {
        this.parent();
        this.set_min_content_height(600);
        
        let box = new Gtk.Box({orientation: Gtk.Orientation.VERTICAL});
        this.add(box);
        
        let shortcutsLabel = new Gtk.Label({ use_markup: true, justify: 2, label: "<b>" + _("Internal shortcuts") + "</b>" + "\n" + "<small>" + _("not editable") + "</small>" });
        let shortcutsFrame = new Gtk.Frame({ valign: 1, shadow_type: 0,
                                    label_widget: shortcutsLabel, label_yalign: 0.5, label_xalign: 0.5,
                                    margin: MARGIN*2 });
        box.add(shortcutsFrame);
        let shortcutsListBox = new Gtk.ListBox({ selection_mode: 0, margin: MARGIN });
        shortcutsFrame.add(shortcutsListBox);
        
        
        let hiddenBox = new Gtk.Box({ margin: MARGIN });
        let hiddenLabel1 = new Gtk.Label({ label: _("Show/hide hidden files") });
        hiddenLabel1.set_halign(1);
        let hiddenLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + <b>H</b>" });
        hiddenBox.pack_start(hiddenLabel1, true, true, 4);
        hiddenBox.pack_start(hiddenLabel2, false, false, 4);
        shortcutsListBox.add(hiddenBox);
        this.addSeparator(shortcutsListBox);
        
        let locationBox = new Gtk.Box({ margin: MARGIN });
        let locationLabel1 = new Gtk.Label({ label: _("Open location dialog") });
        locationLabel1.set_halign(1);
        let locationLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + <b>L</b>" });
        locationBox.pack_start(locationLabel1, true, true, 4);
        locationBox.pack_start(locationLabel2, false, false, 4);
        shortcutsListBox.add(locationBox);
        
        let completionBox = new Gtk.Box({ margin: MARGIN });
        let completionLabel1 = new Gtk.Label({ label: "    - " + _("apply completion") });
        completionLabel1.set_halign(1);
        let completionLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Tab</b>" });
        completionBox.pack_start(completionLabel1, true, true, 4);
        completionBox.pack_start(completionLabel2, false, false, 4);
        shortcutsListBox.add(completionBox);
        
        let historyBox = new Gtk.Box({ margin: MARGIN });
        let historyLabel1 = new Gtk.Label({ label: "    - " + _("browse history") });
        historyLabel1.set_halign(1);
        let historyLabel2 = new Gtk.Label({ use_markup: true, label: "⬆" });
        historyBox.pack_start(historyLabel1, true, true, 4);
        historyBox.pack_start(historyLabel2, false, false, 4);
        shortcutsListBox.add(historyBox);
        this.addSeparator(shortcutsListBox);
        
        let parentBox = new Gtk.Box({ margin: MARGIN });
        let parentLabel1 = new Gtk.Label({ label: _("Go to parent folder") });
        parentLabel1.set_halign(1);
        let parentLabel2 = new Gtk.Label({ use_markup: true, label: _("<b>BackSpace</b>") });
        parentBox.pack_start(parentLabel1, true, true, 4);
        parentBox.pack_start(parentLabel2, false, false, 4);
        shortcutsListBox.add(parentBox);
        
        let homeBox = new Gtk.Box({ margin: MARGIN });
        let homeLabel1 = new Gtk.Label({ label: _("Go to home folder") });
        homeLabel1.set_halign(1);
        let homeLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Alt</b> + ↖" });
        homeBox.pack_start(homeLabel1, true, true, 4);
        homeBox.pack_start(homeLabel2, false, false, 4);
        shortcutsListBox.add(homeBox);
        
        let previousBox = new Gtk.Box({ margin: MARGIN });
        let previousLabel1 = new Gtk.Label({ label: _("Go to previous folder") });
        previousLabel1.set_halign(1);
        let previousLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Alt</b> + ⬅" });
        previousBox.pack_start(previousLabel1, true, true, 4);
        previousBox.pack_start(previousLabel2, false, false, 4);
        shortcutsListBox.add(previousBox);
        this.addSeparator(shortcutsListBox);
        
        let switchLeftBox = new Gtk.Box({ margin: MARGIN });
        let switchLeftLabel1 = new Gtk.Label({ label: _("Switch to left tab") });
        switchLeftLabel1.set_halign(1);
        let switchLeftLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + ⬅  " + "<small>" + _("or") + "</small>" + "  <b>Ctrl</b> + <b>Page</b> ⬇" });
        switchLeftBox.pack_start(switchLeftLabel1, true, true, 4);
        switchLeftBox.pack_start(switchLeftLabel2, false, false, 4);
        shortcutsListBox.add(switchLeftBox);
        
        let switchRightBox = new Gtk.Box({ margin: MARGIN });
        let switchRightLabel1 = new Gtk.Label({ label: _("Switch to right tab") });
        switchRightLabel1.set_halign(1);
        let switchRightLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + <b>Tab</b>  " + "<small>" + _("or") + "</small>" + "  <b>Ctrl</b> + ➡  " + "<small>" + _("or") + "</small>" + "  <b>Ctrl</b> + <b>Page</b> ⬆" });
        switchRightBox.pack_start(switchRightLabel1, true, true, 4);
        switchRightBox.pack_start(switchRightLabel2, false, false, 4);
        shortcutsListBox.add(switchRightBox);
        this.addSeparator(shortcutsListBox);
        
        let moreColumnsBox = new Gtk.Box({ margin: MARGIN });
        let moreColumnsLabel1 = new Gtk.Label({ label: _("Increase max-columns number") });
        moreColumnsLabel1.set_halign(1);
        let moreColumnsLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + ➕" });
        moreColumnsBox.pack_start(moreColumnsLabel1, true, true, 4);
        moreColumnsBox.pack_start(moreColumnsLabel2, false, false, 4);
        shortcutsListBox.add(moreColumnsBox);

        let lessColumnsBox = new Gtk.Box({ margin: MARGIN });
        let lessColumnsLabel1 = new Gtk.Label({ label: _("Decrease max-columns number") });
        lessColumnsLabel1.set_halign(1);
        let lessColumnsLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Ctrl</b> + ➖" });
        lessColumnsBox.pack_start(lessColumnsLabel1, true, true, 4);
        lessColumnsBox.pack_start(lessColumnsLabel2, false, false, 4);
        shortcutsListBox.add(lessColumnsBox);
        
        let moreRowsBox = new Gtk.Box({ margin: MARGIN });
        let moreRowsLabel1 = new Gtk.Label({ label: _("Increase min-rows number") });
        moreRowsLabel1.set_halign(1);
        let moreRowsLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Alt</b> + ➕" });
        moreRowsBox.pack_start(moreRowsLabel1, true, true, 4);
        moreRowsBox.pack_start(moreRowsLabel2, false, false, 4);
        shortcutsListBox.add(moreRowsBox);
        
        let lessRowsBox = new Gtk.Box({ margin: MARGIN });
        let lessRowsLabel1 = new Gtk.Label({ label: _("Decrease min-rows number") });
        lessRowsLabel1.set_halign(1);
        let lessRowsLabel2 = new Gtk.Label({ use_markup: true, label: "<b>Alt</b> + ➖" });
        lessRowsBox.pack_start(lessRowsLabel1, true, true, 4);
        lessRowsBox.pack_start(lessRowsLabel2, false, false, 4);
        shortcutsListBox.add(lessRowsBox);
        this.addSeparator(shortcutsListBox);
        
        let dashBox = new Gtk.Box({ margin: MARGIN });
        let dashLabel1 = new Gtk.Label({ label: _("Show/hide dash in overview") });
        dashLabel1.set_halign(1);
        let dashLabel2 = new Gtk.Label({ use_markup: true, label: "<b>F9</b>" });
        dashBox.pack_start(dashLabel1, true, true, 4);
        dashBox.pack_start(dashLabel2, false, false, 4);
        shortcutsListBox.add(dashBox);
        this.addSeparator(shortcutsListBox);
        
        let refreshBox = new Gtk.Box({ margin: MARGIN });
        let refreshLabel1 = new Gtk.Label({ label: _("Refresh view") });
        refreshLabel1.set_halign(1);
        let refreshLabel2 = new Gtk.Label({ use_markup: true, label: "<b>F5</b>" });
        refreshBox.pack_start(refreshLabel1, true, true, 4);
        refreshBox.pack_start(refreshLabel2, false, false, 4);
        shortcutsListBox.add(refreshBox);
        this.addSeparator(shortcutsListBox);
    },

    addSeparator: function(container) {
        let separatorRow = new Gtk.ListBoxRow({sensitive: false});
        separatorRow.add(new Gtk.Separator({ margin_left: MARGIN, margin_right: MARGIN}));
        container.add(separatorRow);
    }
});

