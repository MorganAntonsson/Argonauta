/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*
 * This code is widely inspired by CustomCorner extension (https://gitlab.com/eccheng/customcorner)
 * which itself is inspired by HotCornDog extension (https://github.com/ricsam/Hot-Corn-Dog)
 */
 
const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const St = imports.gi.St;

const Main = imports.ui.main;
const Layout = imports.ui.layout;
const Tweener = imports.ui.tweener;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;

const monitor = Main.layoutManager.primaryMonitor;
const CORNER_POS = {
    'top-left': { x: 0, y: 0, xAlign: Clutter.ActorAlign.START, yAlign: Clutter.ActorAlign.START, angle: 0, xRipple: 0, yRipple: 0 },
    'top-right': { x: monitor.width-2, y: 0, xAlign: Clutter.ActorAlign.END, yAlign: Clutter.ActorAlign.START, angle: 90, xRipple: monitor.width, yRipple: 0 },
    'bottom-left': { x: 0, y: monitor.height-2, xAlign: Clutter.ActorAlign.START, yAlign: Clutter.ActorAlign.END, angle: 270, xRipple: 0, yRipple: monitor.height },
    'bottom-right': { x: monitor.width-2, y: monitor.height-2, xAlign: Clutter.ActorAlign.END, yAlign: Clutter.ActorAlign.END, angle: 180, xRipple: monitor.width, yRipple: monitor.height }
};

var HotCorner = new Lang.Class({
    Name: 'HotCorner',
    
    _init: function() {
        let c = Convenience.getSettings().get_string('hot-corner');
        this.c = c;

        // Create artificial hot corner
        this.corner = new St.Bin({
            style_class: 'panel-corner',
            reactive: true,
            can_focus: true,
            x_fill: true,
            y_fill: false,
            track_hover: true
        });
        
        // without this style it doesn't work
        this.corner.set_style('visibility:hidden;width:10px;height:10px');

        // Activate custom hot corner and place it on the screen.
        Main.uiGroup.add_actor(this.corner);
        this.corner.set_position(CORNER_POS[c].x, CORNER_POS[c].y);

        // Create chrome actor for wrapping artificial hot corner on screen bottom
        // (otherwise their enter-events get intercepted by the windows below them).
        this.actor = new St.Widget({
            clip_to_allocation: true,
            layout_manager: new Clutter.BinLayout()
        });
        this.actor.add_constraint(new Layout.MonitorConstraint({primary: true, work_area: true}));
        Main.layoutManager.addChrome(this.actor, {affectsInputRegion: false});
        // When the actor has a clip, it somehow breaks drag-and-drop funcionality in
        // the overview. We remove the actor's clip to avoid this.
        this.actor.set_clip(0, 0, 0, 0);

        // Create 'wrapper' for hot corner.
        this.wrap = new St.Widget({x_expand: true, y_expand: true,
                              x_align: CORNER_POS[c].xAlign,
                              y_align: CORNER_POS[c].yAlign});
        this.wrap.set_size(10, 10);

        // Deploy wrappers.
        this.actor.add_actor(this.wrap);
        Main.layoutManager.trackChrome(this.wrap, {affectsInputRegion: true});

        this.corner.cantrigger = true;
        
        this._ripple1 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0, visible: false, rotation_angle_z: CORNER_POS[c].angle });
        this._ripple2 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0, visible: false, rotation_angle_z: CORNER_POS[c].angle });
        this._ripple3 = new St.BoxLayout({ style_class: 'ripple-box', opacity: 0, visible: false, rotation_angle_z: CORNER_POS[c].angle });
        
        Main.layoutManager.uiGroup.add_actor(this._ripple1);
        Main.layoutManager.uiGroup.add_actor(this._ripple2);
        Main.layoutManager.uiGroup.add_actor(this._ripple3);
    },
    
    _animRipple: function(ripple, delay, time, startScale, startOpacity, finalScale) {
        // We draw a ripple by using a source image and animating it scaling
        // outwards and fading away. We want the ripples to move linearly
        // or it looks unrealistic, but if the opacity of the ripple goes
        // linearly to zero it fades away too quickly, so we use Tweener's
        // 'onUpdate' to give a non-linear curve to the fade-away and make
        // it more visible in the middle section.

        ripple._opacity = startOpacity;

        if (ripple.get_text_direction() == Clutter.TextDirection.RTL)
            ripple.set_anchor_point_from_gravity(Clutter.Gravity.NORTH_EAST);

        ripple.visible = true;
        ripple.opacity = 255 * Math.sqrt(startOpacity);
        ripple.scale_x = ripple.scale_y = startScale;

        ripple.x = CORNER_POS[this.c].xRipple;
        ripple.y = CORNER_POS[this.c].yRipple;

        Tweener.addTween(ripple, { _opacity: 0,
                                   scale_x: finalScale,
                                   scale_y: finalScale,
                                   delay: delay,
                                   time: time,
                                   transition: 'linear',
                                   onUpdate: () => { ripple.opacity = 255 * Math.sqrt(ripple._opacity); },
                                   onComplete: () => { ripple.visible = false; } });
    },

    _rippleAnimation: function() {
        // Show three concentric ripples expanding outwards; the exact
        // parameters were found by trial and error, so don't look
        // for them to make perfect sense mathematically

        //                              delay  time  scale opacity => scale
        this._animRipple(this._ripple1, 0.0,   0.83,  0.25,  1.0,     1.5);
        this._animRipple(this._ripple2, 0.05,  1.0,   0.0,   0.7,     1.25);
        this._animRipple(this._ripple3, 0.35,  1.0,   0.0,   0.3,     1);
    },
    
    disable: function() {
        // Remove ripples
        Main.layoutManager.uiGroup.remove_actor(this._ripple1);
        Main.layoutManager.uiGroup.remove_actor(this._ripple2);
        Main.layoutManager.uiGroup.remove_actor(this._ripple3);
        this._ripple1.destroy();
        this._ripple2.destroy();
        this._ripple3.destroy();
        
        // Remove corner wrapper widget.
        Main.layoutManager.removeChrome(this.actor);
        Main.layoutManager.untrackChrome(this.wrap);
        
        // Disconnect the signals which control our custom hot corner,
        // and remove the corner himself.
        if (this._sig_enter_id) {
            this.corner.disconnect(this._sig_enter_id);
            this._sig_enter_id = null;
        }
        if (this._sig_leave_id) {
            this.corner.disconnect(this._sig_leave_id);
            this._sig_leave_id = null;
        }
        this.corner.destroy();
    }
});


