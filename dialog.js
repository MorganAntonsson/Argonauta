/* jslint esversion: 6 */

/*
 * Copyright 2018 Abakkk
 *
 * This file is part of Argonauta, a Files View extension for GNOME Shell
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *This is a fork of Gnome Shell runDialog.js
 *https://gitlab.gnome.org/GNOME/gnome-shell/blob/master/js/ui/runDialog.js
 */
 

const Clutter = imports.gi.Clutter;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Lang = imports.lang;
const St = imports.gi.St;

const Main = imports.ui.main;
const ModalDialog = imports.ui.modalDialog;
const ShellEntry = imports.ui.shellEntry;
const Tweener = imports.ui.tweener;
const History = imports.misc.history;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const Gettext = imports.gettext.domain(Extension.metadata["gettext-domain"]);
const _ = Gettext.gettext;

var DIALOG_GROW_TIME = 0.1;

var _save = function () {
    if (this._history.length > this._limit)
        this._history.splice(0, this._history.length - this._limit);

    if (this._key)
        this.settings.set_strv(this._key, this._history);
};

var LocationDialog = new Lang.Class({
    Name: 'LocationDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function(source) {
        this.source = source;
        this.currentDirectory = this.source.directory;
        this.settings = Convenience.getSettings();
        this.parent({ styleClass: 'run-dialog',
                      destroyOnClose: true });

        this._enableInternalCommands = false;


        let label = new St.Label({ style_class: 'run-dialog-label',
                                   text: _("Enter location") });

        this.contentLayout.add(label, { x_fill: false,
                                        x_align: St.Align.START,
                                        y_align: St.Align.START });

        let entry = new St.Entry({ style_class: 'run-dialog-entry',
                                   can_focus: true });
        ShellEntry.addContextMenu(entry);

        entry.label_actor = label;

        this._entryText = entry.clutter_text;
        this.contentLayout.add(entry, { y_align: St.Align.START });
        this.setInitialKeyFocus(this._entryText);

        this._errorBox = new St.BoxLayout({ style_class: 'run-dialog-error-box' });

        this.contentLayout.add(this._errorBox, { expand: true });

        let errorIcon = new St.Icon({ icon_name: 'dialog-error-symbolic',
                                      icon_size: 24,
                                      style_class: 'run-dialog-error-icon' });

        this._errorBox.add(errorIcon, { y_align: St.Align.MIDDLE });

        this._commandError = false;

        this._errorMessage = new St.Label({ style_class: 'run-dialog-error-label' });
        this._errorMessage.clutter_text.line_wrap = true;

        this._errorBox.add(this._errorMessage, { expand: true,
                                                 x_align: St.Align.START,
                                                 x_fill: false,
                                                 y_align: St.Align.MIDDLE,
                                                 y_fill: false });

        this._errorBox.hide();

        this.setButtons([{ action: Lang.bind(this, this.close),
                           label: _("Close"),
                           key: Clutter.Escape }]);

        this._pathCompleter = new Gio.FilenameCompleter();

        this._history = new History.HistoryManager({ limit: 30, entry: this._entryText });
        this._history._key = 'location-history';
        this._history.settings = this.settings;
        this._history._history = this.settings.get_strv('location-history');
        this._history._save = _save;
        this.settings.connect('changed::location-history', Lang.bind(this, this._historyChanged));
        this._entryText.connect('key-press-event', (o, e) => {
            let symbol = e.get_key_symbol();
            if (symbol == Clutter.Return || symbol == Clutter.KP_Enter) {
                this.popModal();
                this._run(o.get_text());
                if (!this._commandError ||
                    !this.pushModal())
                    this.close();

                return Clutter.EVENT_STOP;
            }
            if (symbol == Clutter.Tab) {
                let text = o.get_text();
                let prefix;
                if (text.lastIndexOf(' ') == -1)
                    prefix = text;
                else
                    prefix = text.substr(text.lastIndexOf(' ') + 1);
                let postfix = this._getCompletion(prefix);
                if (postfix != null && postfix.length > 0) {
                    o.insert_text(postfix, -1);
                    o.set_cursor_position(text.length + postfix.length);
                }
                return Clutter.EVENT_STOP;
            }
            return Clutter.EVENT_PROPAGATE;
        });
    },
    
    _historyChanged: function() {
        this._history._history = this.settings.get_strv(this._history._key);
        this._history._historyIndex = this._history._history.length;
    },

    _getCompletion: function(text) {
        text = this._prepend(text);
        return this._pathCompleter.get_completion_suffix(text);
    },
    
    _prepend: function(text) {
        if (text.indexOf('~/') == 0)
            return GLib.get_home_dir() + text.slice(1);
        if (text.indexOf('/') == 0)
            return text;
        if (text.indexOf('../../../') == 0)
            return this.currentDirectory.get_parent().get_parent().get_parent().get_path() + text.split('../../..')[1];
        if (text.indexOf('../../') == 0)
            return this.currentDirectory.get_parent().get_parent().get_path() + text.split('../..')[1];
        if (text.indexOf('../') == 0)
            return this.currentDirectory.get_parent().get_path() + text.split('..')[1];
        if (text.indexOf('./') == 0)
            return this.currentDirectory.get_path() + text.split('.')[1];
        return this.currentDirectory.get_path() + "/" + text;
    },

    _run: function(path) {
        path = this._prepend(path);
        this._history.addItem(path);
        this._commandError = false;
        
        if (GLib.file_test(path,GLib.FileTest.EXISTS)) {
            let gfile = Gio.File.new_for_path(path);
            if (GLib.file_test(path,GLib.FileTest.IS_DIR))
                this.source.openNewDirectory(gfile);
            else {
                try {
                    Gio.AppInfo.launch_default_for_uri(gfile.get_uri(), global.create_app_launch_context(0, -1));
                    Main.overview.hide();
                } catch(e) {
                    let message = e.message.replace(/[^:]*: *(.+)/, '$1');
                    this._showError(message);
                }
            }
        } else {
            this._showError(_("File or folder not found"));
        }
        
    },

    _showError: function(message) {
        this._commandError = true;

        this._errorMessage.set_text(message);

        if (!this._errorBox.visible) {
            let [errorBoxMinHeight, errorBoxNaturalHeight] = this._errorBox.get_preferred_height(-1);

            let parentActor = this._errorBox.get_parent();
            Tweener.addTween(parentActor,
                             { height: parentActor.height + errorBoxNaturalHeight,
                               time: DIALOG_GROW_TIME,
                               transition: 'easeOutQuad',
                               onComplete: () => {
                                   parentActor.set_height(-1);
                                   this._errorBox.show();
                               }
                             });
        }
    },

    open: function() {
        this._history.lastItem();
        this._errorBox.hide();
        this._entryText.set_text('');
        this._commandError = false;
        this.parent();
    },
});
